<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>EFI System</title>
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(URL::to('/public/icon.ico')); ?>" />
        <!-- Fonts -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

        <!-- Styles -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

        <link href="<?php echo e(URL::to('/public/js/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(URL::to('/public/js/bower_components/metisMenu/dist/metisMenu.min.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(URL::to('/public/js/dist/css/timeline.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(URL::to('/public/js/dist/css/sb-admin-2.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(URL::to('/public/js/bower_components/morrisjs/morris.css')); ?>" rel="stylesheet">
        <link href="<?php echo e(URL::to('/public/js/bower_components/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">

        <?php /* <link href="<?php echo e(elixir('css/app.css')); ?>" rel="stylesheet"> */ ?>

        <style>
            body {
                font-family: 'Lato';
            }
            .dropdown-toggle,.Login{
                margin-right: 15px;
            }
            .fa-btn {
                margin-right: 6px;
            }
            .status-message{
                text-align: center;
                color: white;
                height: 30px;
                border-radius: 5px;
            }
            .error-msg{
                background-color: #c76464;
            }
            .success-msg{
                background-color: #1dd276;
            }
            .message{
                padding-top: 3px;
            }
            .messgae-container
            {
                padding-bottom: 20px;
            }
            .closed{
                -webkit-appearance: none;
                padding: 0;
                cursor: pointer;
                background: 0 0;
                border: 0;
                float: right;
                font-size: 21px;
                font-weight: bold;
                line-height: 1;
                color: #000;
                text-shadow: 0 1px 0 #fff;
                filter: alpha(opacity=20);
                opacity: .2;
            }
        </style>
    </head>
    <body id="app-layout">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="containers">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        EFI System
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <!--                <ul class="nav navbar-nav">
                                        <li><a href="<?php echo e(url('/home')); ?>">Home</a></li>
                                    </ul>-->

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        <?php if(session()->get('Name') == ""): ?>
                        <li><a href="<?php echo e(url('')); ?>" class="Login">Login</a></li>
                        <!--<li><a href="<?php echo e(url('/register')); ?>">Register</a></li>-->
                        <?php else: ?>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                <?php echo e(session()->get('UserName')); ?> <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo e(url('/logout')); ?>"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Navigation -->
        <?php if(session()->get('Name')): ?>
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <?php if(session()->get('Name') == "superadmin"): ?>
                        <li>
                            <a href="<?php echo e(URL::to('/adminlist')); ?>"><i class="fa fa-book fa-list"></i> List Of Admins</a>
                        </li>
                        <?php endif; ?>
                        <?php if(session()->get('Name') == "superadmin" || session()->get('Name') == "admin"): ?>
                        <li>
                            <a href="<?php echo e(URL::to('/superuserlist')); ?>"><i class="fa fa-book fa-list"></i> List Of Super Users</a>
                        </li>
                        <?php endif; ?>
                        <?php if(session()->get('Name') == "superadmin" || session()->get('Name') == "admin" || session()->get('Name') == "superuser"): ?>
                        <li>
                            <a href="<?php echo e(URL::to('/contractorlist')); ?>"><i class="fa fa-book fa-list"></i> List Of Contractors</a>
                        </li>
                        <?php endif; ?>
                        <?php if(session()->get('Name') == "superadmin" || session()->get('Name') == "admin"): ?>
                        <li>
                            <a href="<?php echo e(URL::to('/dashboard')); ?>"><i class="fa fa-user fa-fw"></i> User Managment</a>
                        </li>
                        <?php endif; ?>
                        <li>
                            <a href="<?php echo e(URL::to('/invoices')); ?>"><i class="fa fa-book fa-fw"></i> Invoices</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-print fa-fw"></i> Print Invoice</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        <?php endif; ?>

        <?php echo $__env->yieldContent('content'); ?>

        <!-- JavaScripts -->
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <?php /* <script src="<?php echo e(elixir('js/app.js')); ?>"></script> */ ?>
</body>
</html>
