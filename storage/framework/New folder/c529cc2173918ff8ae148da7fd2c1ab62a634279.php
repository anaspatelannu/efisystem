<?php $__env->startSection('content'); ?>
<!-- DataTables CSS -->
<link href="<?php echo e(URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')); ?>" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<!--<link href="<?php echo e(URL::to('/public/js/bower_components/datatables-responsive/css/dataTables.responsive.css')); ?>" rel="stylesheet">-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Invoices</h1>
            <div class="form-group" style="float: right;">
                <button type="submit" id="fetchinvoice" class="btn btn-primary">
                    <i class="fa fa-btn fa-arrow-down"></i> Fetch Invoice
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Of Invoices
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <?php echo e(csrf_field()); ?>

                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Delivery Date</th>
                                    <th>Vehicle</th>
                                    <th>Amount</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <?php if($List != null): ?>
                            <tbody>
                                <?php foreach($List as $vallist): ?>
                                <tr class="odd gradeX">
                                    <td><?php echo e($vallist->NAME1); ?></td>
                                    <td><?php echo e($vallist->DELDATE); ?></td>
                                    <td><?php echo e($vallist->VEHICLE); ?></td>
                                    <td><?php echo e($vallist->AMOUNT); ?></td>
                                    <td><?php echo e($vallist->QTY); ?></td>
                                    <td><a href="<?php echo e(URL::to('/password/reset?'.$vallist->NAME1)); ?>">Detail</a></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <?php endif; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<script src="<?php echo e(URL::to('/public/js/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<script src="<?php echo e(URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')); ?>"></script>

<script>
$(document).ready(function () {
    $('#dataTables-example').DataTable({
        responsive: true
    });
});


$(document).on('click', '#fetchinvoice', function () {
    $.ajax({
        type: "POST",
        url: "<?php echo e(URL::to('/fetchinvoice')); ?>",
        data: {Fetch:'1',_token: $('input[name=_token]').val()},
        success: function (data) {
            
        }
    });
});

</script>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>