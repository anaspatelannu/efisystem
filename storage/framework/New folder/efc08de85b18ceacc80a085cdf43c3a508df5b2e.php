<?php $__env->startSection('content'); ?>
<!-- DataTables CSS -->
    <link href="<?php echo e(URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')); ?>" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <!--<link href="<?php echo e(URL::to('/public/js/bower_components/datatables-responsive/css/dataTables.responsive.css')); ?>" rel="stylesheet">-->
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Super Users</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Of Super Users
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Vendor Code</th>
                                    <th>Expiry Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($List as $vallist): ?>
                                <tr>
                                    <td><?php echo e($vallist->UserName); ?></td>
                                    <td><?php echo e($vallist->Email); ?></td>
                                    <td><?php echo e($vallist->ContactNo); ?></td>
                                    <td><?php echo e($vallist->VendorCode); ?></td>
                                    <td><?php echo e($vallist->ExpiryDate); ?></td>
                                    <td><a href="<?php echo e(URL::to('/password/reset?'.$vallist->Email)); ?>">Reset Password</a></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>

    <script src="<?php echo e(URL::to('/public/js/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')); ?>"></script>
    
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>