<?php $__env->startSection('content'); ?>
<!-- DataTables CSS -->
<link href="<?php echo e(URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')); ?>" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<!--<link href="<?php echo e(URL::to('/public/js/bower_components/datatables-responsive/css/dataTables.responsive.css')); ?>" rel="stylesheet">-->

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Invoices</h1>
            <div class="form-group" style="float: right;">
                <button type="submit" id="fetchinvoice" class="btn btn-primary">
                    <i class="fa fa-btn fa-arrow-down"></i> Fetch Invoice
                </button>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    List Of Invoices
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <?php echo e(csrf_field()); ?>

                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Delivery Date</th>
                                    <th>Vehicle</th>
                                    <th>Amount</th>
                                    <th>Quantity</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="invoice">
                                <?php if($List != null): ?>
                                <?php foreach($List as $vallist): ?>
                                <tr>
                                    <td><?php echo e($vallist->NAME1); ?></td>
                                    <td><?php echo e($vallist->DELDATE); ?></td>
                                    <td><?php echo e($vallist->VEHICLE); ?></td>
                                    <td><?php echo e($vallist->AMOUNT); ?></td>
                                    <td><?php echo e($vallist->QTY); ?></td>
                                    <td><a href="#" class="detail" id="<?php echo e($vallist->ID); ?>">Detail</a></td><!-- <?php echo e(URL::to('/password/reset?'.$vallist->NAME1)); ?> -->
                                </tr>
                                <?php endforeach; ?>
                                <?php else: ?>
                                <tr>
                                    <td valign="top" colspan="6" class="dataTables_empty">No data available in table</td>
                                </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" style="overflow-y: scroll">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>

                <div style="text-align: center" id="invoicedetail">

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <?php $__env->stopSection(); ?>


    <script src="<?php echo e(URL::to('/public/js/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js')); ?>"></script>
    <script src="<?php echo e(URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')); ?>"></script>

    <script>
$(document).ready(function () {
    //    $('#dataTables-example').DataTable({
    //        responsive: true
    //    });
    <?php if($Param == 1): ?>
            $("#fetchinvoice").attr('disabled', 'true')
            <?php endif; ?>
});


$(document).on('click', '#fetchinvoice', function () {
    $.ajax({
        type: "POST",
        url: "<?php echo e(URL::to('/fetchinvoice')); ?>",
        data: {Fetch: '1', _token: $('input[name=_token]').val()},
        success: function (data) {
            var Data = jQuery.parseJSON(data);
            console.log(Data)
            if (Data != "") {
                $("#invoice").find('tr').remove()
                for (var i = 0; i < Data.length; i++) {
                    var HTML = "";
                    HTML = HTML + "<tr>";
                    HTML = HTML + "<td>" + Data[i].NAME1 + "</td>";
                    HTML = HTML + "<td>" + Data[i].DELDATE + "</td>";
                    HTML = HTML + "<td>" + Data[i].VEHICLE + "</td>";
                    HTML = HTML + "<td>" + Data[i].AMOUNT + "</td>";
                    HTML = HTML + "<td>" + Data[i].QTY + "</td>";
                    HTML = HTML + "<td><a href='#' class='detail' id='" + Data[i].ID + "'>Detail</a></td>";
                    HTML = HTML + "</tr>";
                    //<?php echo e(URL::to('/password/reset?')); ?>"+Data[i].NAME1+"
                    $("#invoice").append(HTML);
                }
            }
            else {
                $("#invoice").find('tr').remove()
                var HTML = "";
                HTML = HTML + "<tr><td valign='top' colspan='6' class='dataTables_empty'>No Invoice</td></tr>";
                $("#invoice").append(HTML);
            }
            $("#fetchinvoice").attr('disabled', 'true')
        }
    });
});

$(document).on('click', '.detail', function (e)
{
    var id = $(this).attr('id');
    $('#invoicedetail').contents().remove();
    $.ajax({
        type: "POST",
        url: "<?php echo e(URL::to('/fetchinvoicedetail')); ?>",
        data: {id: id, _token: $('input[name=_token]').val()},
        success: function (data) {
            var Data = jQuery.parseJSON(data);
            var HTML = "";

            HTML = HTML + "<h1>Detail</h1>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Name</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.NAME1 + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>LIFNR</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.LIFNR + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>SH Number</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.SHNUMBER + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Delivery Date</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.DELDATE + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>VBELN</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.VBELN + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Vehicle</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.VEHICLE + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Amount</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.AMOUNT + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Currency</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.CURR + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Quantity</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.QTY + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Unit</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.UNIT + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>Acknowldge</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.ACK + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>F1</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.F1 + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>F2</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.F2 + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>F3</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.F3 + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>F4</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.F4 + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<div class='col-lg-12 form-group'>";
            HTML = HTML + "<div class='form-group'>";
            HTML = HTML + "<div class='col-lg-2'>";
            HTML = HTML + "<p style='float:left'>F5</p>";
            HTML = HTML + "</div>";
            HTML = HTML + "<div class='col-lg-10'>";
            HTML = HTML + "<input type='text' class='form-control' value='" + Data.F5 + "' disabled='true' />";
            HTML = HTML + "</div></div></div>";

            HTML = HTML + "<br>&nbsp;";
            $("#invoicedetail").append(HTML);
        }
    });

    $('.modal-footer').on('click', function (e) {
        $("#myModal a.btn").off("click");
        console.log("1");
    });
    $("#myModal").on("show", function () {    // wire up the OK button to dismiss the modal when shown
        $("#myModal a.btn").on("click", function (e) {
            console.log("button pressed"); // just as an example...
            $("#myModal").modal('hide'); // dismiss the dialog

        });
    });
    $("#myModal").on("hide", function () {    // remove the event listeners when the dialog is dismissed
        $("#myModal a.btn").off("click");
        console.log("1");
    });
    $("#myModal").on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
        $("#myModal").remove();
        console.log("1");
    });
    $("#myModal").modal({// wire up the actual modal functionality and show the dialog
        "backdrop": "static",
        "keyboard": true,
        "show": true                     // ensure the modal is shown immediately
    });
});

    </script>


<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>