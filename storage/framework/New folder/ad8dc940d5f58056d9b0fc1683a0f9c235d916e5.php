<?php $__env->startSection('content'); ?>

<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User Mangment</h1>
            </div>
            <div>
                <div class="containers">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel-default">

                                <div class="panel-body">
                                    <div class="messgae-container" style="display: none">
                                        <div class="status-message error-msg">
                                            <p class="message"></p>
                                        </div>
                                    </div>
                                    <form class="form-horizontal" role="form" method="POST" action="<?php echo e(url('/createuser')); ?>">
                                        <?php echo e(csrf_field()); ?>


                                        <div class="form-group<?php echo e($errors->has('usertype') ? ' has-error' : ''); ?>">
                                            <label for="usertype" class="col-md-4 control-label">User Type</label>

                                            <div class="col-md-6">
                                                <!--<input id="usertype" type="number" class="form-control" name="usertype" value="<?php echo e(old('usertype')); ?>">-->
                                                <?php if(session()->get('Name') == "superadmin"): ?>
                                                <select id="usertype" class="form-control" name="usertype" value="<?php echo e(old('usertype')); ?>">
                                                    <option value="admin">Admin</option>
                                                    <option value="superuser">SuperUser</option>
                                                    <option value="contractor">Contractor</option>
                                                </select>
                                                <?php elseif(session()->get('Name') == "admin"): ?>
                                                <select id="usertype" class="form-control" name="usertype" value="<?php echo e(old('usertype')); ?>">
                                                    <option value="superuser">SuperUser</option>
                                                    <option value="contractor">Contractor</option>
                                                </select>
                                                <?php endif; ?>
                                                <?php if($errors->has('usertype')): ?>
                                                <span class="help-block">
                                                    <strong><?php echo e($errors->first('usertype')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group<?php echo e($errors->has('name') ? ' has-error' : ''); ?>">
                                            <label for="username" class="col-md-4 control-label">User Name</label>

                                            <div class="col-md-6">
                                                <input id="username" type="text" class="form-control" required="true" name="username" value="<?php echo e(old('username')); ?>">

                                                <?php if($errors->has('username')): ?>
                                                <span class="help-block">
                                                    <strong><?php echo e($errors->first('username')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group<?php echo e($errors->has('email') ? ' has-error' : ''); ?>">
                                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" required="true" class="form-control" name="email" value="<?php echo e(old('email')); ?>">

                                                <?php if($errors->has('email')): ?>
                                                <span class="help-block">
                                                    <strong><?php echo e($errors->first('email')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group<?php echo e($errors->has('password') ? ' has-error' : ''); ?>">
                                            <label for="password" class="col-md-4 control-label">Password</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" required="true" class="form-control" name="password">

                                                <?php if($errors->has('password')): ?>
                                                <span class="help-block">
                                                    <strong><?php echo e($errors->first('password')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group<?php echo e($errors->has('vendorcode') ? ' has-error' : ''); ?>">
                                            <label for="vendorcode" class="col-md-4 control-label">Vendor Code</label>

                                            <div class="col-md-6">
                                                <input id="vendorcode" type="text" class="form-control" name="vendorcode" value="<?php echo e(old('vendorcode')); ?>">

                                                <?php if($errors->has('vendorcode')): ?>
                                                <span class="help-block">
                                                    <strong><?php echo e($errors->first('vendorcode')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group<?php echo e($errors->has('contactnumber') ? ' has-error' : ''); ?>">
                                            <label for="contactnumber" class="col-md-4 control-label">Contact Number</label>

                                            <div class="col-md-6">
                                                <input id="contactnumber" type="number" required="true" class="form-control" name="contactnumber" value="<?php echo e(old('contactnumber')); ?>">

                                                <?php if($errors->has('contactnumber')): ?>
                                                <span class="help-block">
                                                    <strong><?php echo e($errors->first('contactnumber')); ?></strong>
                                                </span>
                                                <?php endif; ?>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" id="create" class="btn btn-primary">
                                                    <i class="fa fa-btn fa-plus-square"></i> Create
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
<script>
$(document).on('focusout', '#email', function () {
    if($('.messgae-container p').text() == "User Name Already Exists"){
        return false;
    }
    var Email = $(this).val();
    $.ajax({
        type: "POST",
        url: "<?php echo e(URL::to('/checkemail')); ?>",
        data: {Email: Email, _token: $('input[name=_token]').val()},
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $('.messgae-container p').text('Email Already Exists')
                $('.messgae-container').show();
                $('#create').attr('disabled','true');
                $("#email").focus();
                return false;
            }
            else {
                $('.messgae-container').hide();
                $('.messgae-container p').text('');
                $('#create').removeAttr('disabled');
            }
        }
    });
});

$(document).on('focusout', '#username', function (e) {
    if($('.messgae-container p').text() == "Email Already Exists"){
        return false;
    }
    var UserName = $(this).val();
    $.ajax({
        type: "POST",
        url: "<?php echo e(URL::to('/checkusername')); ?>",
        data: {UserName: UserName, _token: $('input[name=_token]').val()},
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $('.messgae-container p').text('User Name Already Exists')
                $('.messgae-container').show();
                $('#create').attr('disabled','true');
                $("#username").focus();
                e.preventDefault();
                return false;
            }
            else {
                $('.messgae-container').hide();
                $('.messgae-container p').text('');
                $('#create').removeAttr('disabled');
            }
        }
    });
});
</script>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>