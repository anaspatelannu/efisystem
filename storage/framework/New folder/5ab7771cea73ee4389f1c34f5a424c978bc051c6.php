<?php $__env->startSection('content'); ?>
<link href="<?php echo e(URL::to('/public/js/bower_components/bootstrap/dist/css/bootstrap.min.css')); ?>" rel="stylesheet">
<link href="<?php echo e(URL::to('/public/js/bower_components/metisMenu/dist/metisMenu.min.css')); ?>" rel="stylesheet">
<link href="<?php echo e(URL::to('/public/js/dist/css/timeline.css')); ?>" rel="stylesheet">
<link href="<?php echo e(URL::to('/public/js/dist/css/sb-admin-2.css')); ?>" rel="stylesheet">
<link href="<?php echo e(URL::to('/public/js/bower_components/morrisjs/morris.css')); ?>" rel="stylesheet">
<link href="<?php echo e(URL::to('/public/js/bower_components/font-awesome/css/font-awesome.min.css')); ?>" rel="stylesheet" type="text/css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">26</div>
                            <div>Admins</div>
                        </div>
                    </div>
                </div>
<!--                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">12</div>
                            <div>Super Users</div>
                        </div>
                    </div>
                </div>
<!--                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">124</div>
                            <div>Contractors</div>
                        </div>
                    </div>
                </div>
<!--                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-book fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">13</div>
                            <div>Invoices</div>
                        </div>
                    </div>
                </div>
<!--                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<script src="<?php echo e(URL::to('/public/js/bower_components/jquery/dist/jquery.min.js')); ?>"></script>
<script src="<?php echo e(URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js')); ?>"></script>
<script src="<?php echo e(URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')); ?>"></script>
<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>