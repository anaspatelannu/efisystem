<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Logs extends Model {

    protected $table = 'logs';
    
    public function InsertLogs($Action_By,$UserName,$Time,$Description){
        $Insert = DB::table('logs')->insert([
            ['Action_By' => $Action_By, 'UserName' => $UserName, 'Time' => $Time, 'Description' => $Description]
        ]);
    }
    
     public function getAll() {
        $Select = DB::table('logs')
                ->orderBy('ID', 'desc')
                ->get();
        return $Select;
    }

}
