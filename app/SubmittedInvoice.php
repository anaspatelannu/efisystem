<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class SubmittedInvoice extends Model
{
    protected $table = 'submitted_invoice';
    
    public function InsertSubmittedInvoice($UserName, $ShipmentNo, $SapInvoice) {
        $Insert = DB::table('submitted_invoice')->insert([
            ['UserName' => $UserName, 'SHNUMBER' => $ShipmentNo, 'SAPINVOICE' => $SapInvoice]
        ]);
    }
    
     public function GetAllInvoice() {
        $Select = DB::table('submitted_invoice')
                ->get();
        return $Select;
    }
    
    public function GetInvoiceByContractorID($UserName) {
        $Select = DB::table('submitted_invoice')
                ->where('UserName', '=', $UserName)
                ->get();
        return $Select;
    }
    
    public function UpdateSubmittedInvoice($ShipmentID,$ShipmentNo,$CancelNumber,$CancelDate){
        DB::table('submitted_invoice')
                ->where('ID', $ShipmentID)
                ->update(['CancelNumber' => $CancelNumber , 'CancelDate' => $CancelDate, 'IsCancel' => 1]);
    }
    
     public static function GetInvoiceCount(){
        $users = DB::table('submitted_invoice')
                     ->select(DB::raw('count(*) as Invoice_Count'))
                     //->where('IsCancel', 0)
                     ->first();
         return $users->Invoice_Count;
    }
    
    public static function GetInvoiceCountByUserName($UserName) {
        $users = DB::table('submitted_invoice')
                     ->select(DB::raw('count(*) as Invoice_Count'))
                     ->where('UserName', '=', $UserName)
                     ->first();
         return $users->Invoice_Count;
    }
}
