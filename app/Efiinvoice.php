<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Efiinvoice extends Model {

    protected $table = 'efiinvoice';
    
    public function GetInvoiceByName($Name) {
        $Select = DB::table('efiinvoice')
                ->where('NAME1', '=', $Name)
                ->get();
        return $Select;
    }
    
    public function GetInvoiceByID($InvoiceID){
        $Select = DB::table('efiinvoice')
                ->where('ID', '=', $InvoiceID)
                ->first();
        return $Select;
    }
    
    public function GetAllInvoice() {
        $Select = DB::table('efiinvoice')
                ->get();
        return $Select;
    }
    
    public static function GetInvoiceCountByUserName($UserName){
        $users = DB::table('efiinvoice')
                     ->select(DB::raw('count(*) as Invoice_Count'))
                     ->where('NAME1', '=', $UserName)
                     ->first();
         return $users->Invoice_Count;
    }
    
    public static function GetInvoiceCount(){
        $users = DB::table('efiinvoice')
                     ->select(DB::raw('count(*) as Invoice_Count'))
                     ->first();
         return $users->Invoice_Count;
    }

    public static function GetFilteredInvoices($UserName, $fromDate, $toDate, $contractorId, $shipmentNo){
        $Select = DB::table('efiinvoice')
//            ->join('users', 'efiinvoice.NAME1', '=', 'users.UserName')
//            ->where('users.ID', '=', $contractorId)
            ->where('efiinvoice.NAME1', '=', $contractorId)
            ->whereBetween('efiinvoice.DELDATE', array($fromDate, $toDate))
            ->get();
        return $Select;
    }
    
    public function InsertInvoice($LIFNR,$NAME1,$SHNUMBER,$DELDATE,$VBELN,$VEHICLE,$AMOUNT,$CURR,$QTY,$UNIT,$ACK,$F1,$F2,$F3,$F4,$F5,$RBLGP,$RandomNum){
        $Insert = DB::table('efiinvoice')->insert([
            ['LIFNR' => $LIFNR, 'NAME1' => $NAME1, 'SHNUMBER' => $SHNUMBER,'DELDATE'=>$DELDATE,'VBELN'=>$VBELN,'VEHICLE'=>$VEHICLE,'AMOUNT'=>$AMOUNT,'CURR'=>$CURR,'QTY'=>$QTY,'UNIT'=>$UNIT,'ACK'=>$ACK,'F1'=>$F1,'F2'=>$F2,'F3'=>$F3,'F4'=>$F4,'F5'=>$F5,'RBLGP'=>$RBLGP,'RandomNum'=>$RandomNum]
        ]);
    }
    
    public function UpdateInvoice($RandomNum,$SAPINVOICE){
         DB::table('efiinvoice')
                ->where('RandomNum', $RandomNum)
                ->update(['SAPINVOICE' => $SAPINVOICE]);
    }
    
     public function GetInvoiceBySAPNUM($SAPINVOICE){
         $Select = DB::table('efiinvoice')
                ->where('SAPINVOICE', '=', $SAPINVOICE)
                ->get();
        return $Select;
    }
    
    public function GetSHNUMBySAPNUM($SAPINVOICE){
         $Select = DB::table('efiinvoice')
                ->where('SAPINVOICE', '=', $SAPINVOICE)->pluck('SHNUMBER');
        return $Select;
    }

}
