<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Users extends Model {

    protected $table = 'users';

    public function GetUserByEmailAndPassword($Email, $Password) {
        $Select = DB::table('users')
                ->where('Email', '=', $Email)
                ->Where('Password', '=', $Password)
                ->Where('Enable/Disable', '=', 0)
                ->first();
        return $Select;
    }

    public function InsertUser($UserType, $UserName, $Email, $Password, $VendorCode, $ContactNumber, $ExpiryDate) {
        $Insert = DB::table('users')->insert([
            ['UserType' => $UserType, 'UserName' => $UserName, 'Email' => $Email, 'Password' => $Password, 'VendorCode' => $VendorCode, 'ContactNo' => $ContactNumber, 'Enable/Disable' => 0, 'ExpiryDate' => $ExpiryDate]
        ]);
    }

    public function GetUserByEmail($Email) {
        $Select = DB::table('users')
                ->where('Email', '=', $Email)
                ->first();
        return $Select;
    }
    
    public function GetUserByUserName($UserName) {
        $Select = DB::table('users')
                ->where('UserName', '=', $UserName)
                ->first();
        return $Select;
    }
    
    public function GetUserByVendorCode($VendorCode) {
        $Select = DB::table('users')
                ->where('VendorCode', '=', $VendorCode)
                ->first();
        return $Select;
    }
   
    public function DisableUser($UserID) {
             DB::table('users')
                ->where('ID', $UserID)
                ->update(['Enable/Disable' => 1]);
    }
    
    public function EnableUser($UserID) {
             DB::table('users')
                ->where('ID', $UserID)
                ->update(['Enable/Disable' => 0]);
    }
    
    public function GetUserByUserID($UserID) {
        $Select = DB::table('users')
                ->where('ID', '=', $UserID)
                ->first();
        return $Select;
    }

    public function UpdateExpiryDate($ID, $ExpiryDate,$NewPassword) {
        DB::table('users')
                ->where('ID', $ID)
                ->update(['ExpiryDate' => $ExpiryDate,'Password'=>$NewPassword]);
    }

    public function getAll($UserType) {
        $Select = DB::table('users')
                ->where('UserType', '=', $UserType)
                ->get();
        return $Select;
    }
    
    public static function GetAdminCount(){
        $users = DB::table('users')
                     ->select(DB::raw('count(*) as Admin_Count'))
                     ->where('UserType', '=', 'admin')
                     ->first();
         return $users->Admin_Count;
    }
    
    public static function GetSuperUserCount(){
        $users = DB::table('users')
                     ->select(DB::raw('count(*) as SuperUser_Count'))
                     ->where('UserType', '=', 'superuser')
                     ->first();
         return $users->SuperUser_Count;
    }
    
    public static function GetContractorCount(){
        $users = DB::table('users')
                     ->select(DB::raw('count(*) as Contractor_Count'))
                     ->where('UserType', '=', 'contractor')
                     ->first();
         return $users->Contractor_Count;
    }
    
    public function UpdateTokenByID($ID, $Token) {
        DB::table('users')
                ->where('ID', $ID)
                ->update(['Token' => $Token]);
    }
    
    public function GetUserByToken($Token) {
        $Select = DB::table('users')
                ->where('Token', '=', $Token)
                ->first();
        return $Select;
    }
    
    public function UpdateLoginTime($ID) {
        DB::table('users')
                ->where('ID', $ID)
                ->update(['LoginTime' => time()]);
    }
    
    public function GetUserLoginTime($ID){
         $Select = DB::table('users')
                ->where('ID', '=', $ID)
                ->first();
        return $Select->LoginTime;
    }
    
    public function ResetLoginTime($ID) {
        DB::table('users')
                ->where('ID', $ID)
                ->update(['LoginTime' => '']);
    }

}
