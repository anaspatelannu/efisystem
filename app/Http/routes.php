<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('/login');
//});

Route::auth();

//Route::get('/home', 'HomeController@index');
Route::get('/dashboard', 'DashboardController@dashboard');
Route::post('/createuser', 'DashboardController@createuser');
Route::get('/login', 'Auth\AuthController@login');
Route::get('/logout', 'DashboardController@logout');
Route::post('/password/email', 'DashboardController@resetpassword');
Route::post('/checkemail', 'DashboardController@checkemail');
Route::post('/checkusername', 'DashboardController@checkusername');
Route::post('/CheckVendorCode', 'DashboardController@CheckVendorCode');
Route::get('/adminlist', 'DashboardController@adminlist');
Route::get('/superuserlist', 'DashboardController@superuserlist');
Route::get('/contractorlist', 'DashboardController@contractorlist');
Route::get('/invoices', 'DashboardController@invoices');
Route::post('/fetchinvoice', 'DashboardController@fetchinvoice');
Route::post('/fetchinvoicedetail', 'DashboardController@fetchinvoicedetail');
Route::get('', 'Auth\AuthController@login');
Route::get('/wsdl','WsdlController@index');
Route::post('/wsdl','WsdlController@index');
Route::post('/SubmitInvoice','DashboardController@SubmitInvoice');
Route::post('/cancelsubmittedinvoice','DashboardController@cancelsubmittedinvoice');
Route::get('/submittedinvoices','DashboardController@submittedinvoices');
Route::get('/printinvoice','DashboardController@printinvoice');
Route::post('/fetchpdfinvoices','DashboardController@fetchpdfinvoices');
Route::post('/SubmittedInvoiceDetail','DashboardController@SubmittedInvoiceDetail');
Route::post('/EditContractor','DashboardController@EditContractor');
Route::post('/UpdateContractor','DashboardController@UpdateContractor');
Route::post('/DisableUser','DashboardController@DisableUser');
Route::post('/EnableUser','DashboardController@EnableUser');

Route::get('/welcome', 'DashboardController@welcome');
Route::get('/Logs', 'DashboardController@Logs');
Route::get('/getInvoice', 'DashboardController@getInvoice');

Route::get('/GetInvoices','WsdlController@GetInvoices');
Route::get('/404', function () {
    return view('/errors/404');
});

Route::post('/UpdateJSON','DashboardController@UpdateJSON');

Route::get('/ForgotPassword', function () {
    $get_params = $_SERVER['QUERY_STRING'];
    $ID = explode("=",$get_params);
    return view('ForgotPassword',['Msg'=>$Msg="",'Msg1'=>$Msg1="",'ID'=>$ID[1]]);
});
Route::post('/ForgotPassword', 'DashboardController@ForgotPassword');

Route::get('/ChangePassword', function () { 
    $get_params = $_SERVER['QUERY_STRING'];
    $get_params = explode("&",$get_params);
    $ID = explode("=",$get_params[0]);
    $Email = explode("=",$get_params[1]);
    return view('ChangePassword',['Msg'=>$Msg="",'Msg1'=>$Msg1="",'Email'=>$Email[1],'ID'=>$ID[1]]);
});
Route::post('/ChangePassword', 'DashboardController@ChangePassword');
Route::post('/UpdateLoginTime', 'DashboardController@UpdateLoginTime');

Route::get('/ForgotEmail', function () {
    $Url = URL::to('/ChangePassword?Email=contractor1@mailinator.com');
    return view('/auth/emails/ForgotPassword',['Url' => $Url]);
});

Route::get('/ResetEmail', function () {
    $Password = 123456;
    return view('/auth/emails/ResetPassword',['Password' => $Password]);
});

Route::get('/AdminResetEmail', function () {
    $Users = new stdClass();
    $Users->UserName = "Contractor1";
    $Users->Email = "Contractor1@mailinator.com";
    $Password = 123456;
    return view('/auth/emails/AdminResetPassword',['Users'=>$Users,'Password' => $Password]);
});

