<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\App;
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;
use Artisaninweb;


class WsdlController extends Controller
{
    public function index(){
        return view('wsdl');
    }
    
    public function GetInvoices(){ 
        SoapWrapper::add(function ($service) {
            $service
                ->name('MI_MM_INVOICE_ACK')
                ->wsdl(base_path()."/MI_MM_INVOICE_ACK.wsdl")
                ->trace(true)                                                   // Optional: (parameter: true/false)
                //->header()                                                      // Optional: (parameters: $namespace,$name,$data,$mustunderstand,$actor)
                //->customHeader($customHeader)                                   // Optional: (parameters: $customerHeader) Use this to add a custom SoapHeader or extended class                
                //->cookie()                                                      // Optional: (parameters: $name,$value)
                //->location()                                                    // Optional: (parameter: $location)
                //->certificate()                                                 // Optional: (parameter: $certLocation)
                ->cache(WSDL_CACHE_NONE)                                        // Optional: Set the WSDL cache
                ->options(['login' => 'pisuper', 'password' => 'lotus123']);   // Optional: Set some extra options
        });

        $data = [
            "ACTTYPE" => "SR",
            "VENDOR" => "7773000586",
            "DATEFROM" => "01.06.2013",
            "DATETO" => "15.06.2013"
        ];

        // Using the added service
        SoapWrapper::service('MI_MM_INVOICE_ACK', function ($service) use ($data) {
            var_dump($service->getFunctions());
            var_dump($service->call('MI_MM_INVOICE_ACK', [$data]));
        });
/*        
//WSDL File
        $wsdl = base_path()."/MI_MM_INVOICE_ACK.wsdl";
        echo "WSDL Base Path:".base_path()."/MI_MM_INVOICE_ACK.wsdl";

        //Login Cardentials
        $paramArray = array("login" => "pisuper",
            "password" => "lotus123",
            "CLIENT" => "500");

        //Connetion To Web Service
        //$client = new SoapClient($wsdl, $paramArray);
        // Add a new service to the wrapper
        SoapWrapper::add(function ($service) {
            $service
                ->name('currency')
                ->wsdl($wsdl)
                ->trace(true)                                                   // Optional: (parameter: true/false)
                ->header()                                                      // Optional: (parameters: $namespace,$name,$data,$mustunderstand,$actor)
                ->customHeader($customHeader)                                   // Optional: (parameters: $customerHeader) Use this to add a custom SoapHeader or extended class                
                ->cookie()                                                      // Optional: (parameters: $name,$value)
                ->location()                                                    // Optional: (parameter: $location)
                ->certificate()                                                 // Optional: (parameter: $certLocation)
                ->cache(WSDL_CACHE_NONE)                                        // Optional: Set the WSDL cache
                ->options(["login" => "pisuper",
            "password" => "lotus123",
            "CLIENT" => "500"]);   // Optional: Set some extra options
        });
        //$client = new SoapClient("http://footballpool.dataaccess.eu/data/info.wso?wsdl");
        //$result = $client->TopGoalScorers(array('iTopN'=>5));
        //var_dump($result);exit;

        var_dump($client);
        //Input To Function
        $parameters = array("ACTTYPE" => "SR",
                            "VENDOR" => "7773000586",
                            "DATEFROM" => "01.06.2013",
                            "DATETO" => "15.06.2013");
        var_dump($parameters);echo 'asad';exit;
        // Function Call // uncomment while uploading on server
        $response = $client->MI_MM_INVOICE_ACK($parameters);
        var_dump($response);*/
    }
}
