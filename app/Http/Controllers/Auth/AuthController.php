<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Users;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers,
    ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|max:255',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data) {
        return User::create([
                    'name' => $data['name'],
                    'email' => $data['email'],
                    'password' => bcrypt($data['password']),
        ]);
    }

    public function login(Request $request) {

        //Checking Logged in user
        $UserID = session()->get('ID');
        if ($UserID):
            return redirect('/welcome');
        endif;
        $Login = 1;
        $Msg = "";
        $temp = "Enable/Disable";
        $get_params = $_SERVER['QUERY_STRING'];
        
        if($get_params):
            $get_params = explode("=",$get_params);
            if ($get_params[1] == 1):
                $Msg = "Your Password Has Been Reset.Please Login To Continue!";
                $Login = 2;
            else:
                $Msg = "Your Password Has Been Changed.Please Login To Continue!";
                $Login = 2;
            endif;
        endif;
        //checking Form Post
        if ($request->input()): 
            $Email = $request->input('email');
            $Password = $request->input('password');
            $UsersModel = new Users();
            $Users = $UsersModel->GetUserByEmailAndPassword($Email, $Password);
            if ($Users != null):
                $ExpiryDate = strtotime($Users->ExpiryDate);
                if ($Users->UserType != "superadmin"):
                    if ($ExpiryDate < time()):
                        $Msg = "Please Reset Your Password";
                        $Login = 0;
                    endif;
                endif;
                if ($Login == 1 && $Users->$temp == 0):
                    $LoginTime = $UsersModel->GetUserLoginTime($Users->ID);
                    $Time = time() - 600;
                    if($LoginTime == "" || $LoginTime <= $Time):
                        session()->put('ID', $Users->ID);
                        session()->put('Name', $Users->UserType);
                        session()->put('UserName', $Users->UserName);
                        $UsersModel->UpdateLoginTime($Users->ID);
                        return redirect('/welcome');
                    else:
                        $Msg = "You are logged in from another device.Please logout and try again";
                        $Login = 3;
                    endif;
                endif;
                if($Users->$temp == 1):
                    $Msg = "Vendor Disabled, Please contact Logistics Department PSO for further inquiry";
                    $Login = 3;
                endif;
            else:
                if ($Login != 2):
                    $Msg = "Please Enter Valid Email or Password";
                    $Login = 3; 
                endif;
            endif;
        endif;

        //Default View
        return view('auth/login', ['Login' => $Login, 'Msg' => $Msg]);
    }

    public function register() {
        echo 'reigister';
        exit;
    }

}
