<?php

namespace App\Http\Controllers;

use Log;
use App\User;
use App\Users;
use App\Efiinvoice;
use Validator;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use SoapClient;
use stdClass;
use Exception;
use SoapFault;
use File;
use App\SubmittedInvoice;
use App\Logs;
use Mail;
use URL;


class DashboardController extends Controller {
    
    public function welcome() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        if ($UserID == null):
            return redirect('');
        endif;

        $UserType = session()->get('Name');
        $UserName = session()->get('UserName');
        $Data = array();
        $Data['UserType'] = $UserType;
        $Data['Admin'] = Users::GetAdminCount();
        $Data['SuperUser'] = Users::GetSuperUserCount();
        $Data['Contractor'] = Users::GetContractorCount();
        if ($UserType == "superadmin" || $UserType == "admin" || $UserType == "superuser"):
            $Data['Invoice'] = SubmittedInvoice::GetInvoiceCount();
        else:
            $Data['Invoice'] = SubmittedInvoice::GetInvoiceCountByUserName($UserName);
        endif;
        return view('welcome', ['Data' => $Data]);
    }

    public function dashboard(Request $request) {

        //Checking Logged in user
        $UserID = session()->get('ID');
        if ($UserID == null):
            return redirect('');
        endif;
        $Param = session()->get('UserInsert');
       
        if($Param):
            $Msg = "User created successfully!";
            session()->forget('UserInsert');
        else:
            $Msg = "";
        endif;
        
        $request->flash();
        //default view
        return view('dashboard',['Msg'=> $Msg]);
    }

    public function createuser(Request $request) {

        //Checking Logged in user
        $UserID = session()->get('ID');
        if ($UserID == null):
            return redirect('');
        endif;
        $UserType = session()->get('Name');
        $UserNames = session()->get('UserName');
        $Time = time() + 18000;
        $LogModel = new Logs();
        //checking Form Post
        if ($request->input()):
            $UserType = $request->input('usertype');
            $UserName = $request->input('username');
            $Email = $request->input('email');
            $Password = $request->input('password');
            $VendorCode = $request->input('vendorcode');
            $Address = $request->input('address');
            $ContactNumber = $request->input('contactnumber');
            $ExpiryDate = date("Y-m-d", (time() + 2.592e+6));
            $UsersModel = new Users();
            
            $Description = $UserNames."(".$_SERVER['REMOTE_ADDR'].") Created user ".$UserName." at ".date("Y-m-d H:i:s", $Time);
            $LogModel = new Logs();
            $LogModel->InsertLogs($UserNames,$UserName,date("Y-m-d H:i:s", $Time),$Description);
            
            $Insert = $UsersModel->InsertUser($UserType, $UserName, $Email, $Password, $VendorCode, $Address, $ContactNumber, $ExpiryDate);
            session()->put('UserInsert', 1);
            $request->flash();
            return redirect('/dashboard');

        endif;
    }

    public function logout() {
        $UserID = session()->get('ID');
        session()->flush();
        $UsersModel = new Users();
        $UsersModel->ResetLoginTime($UserID);
        return redirect('');
    }

    public function resetpassword(Request $request) {
        //checking Form Post
        ini_set('xdebug.max_nesting_level', 2000);
        $Param = $_SERVER['QUERY_STRING'];
        if ($request->input()):
            //print_r($request->input());exit;
            $Email = $request->input('email');
            $Password = $request->input('password');
            $ExpiryDate = date("Y-m-d", (time() + 2.592e+6));

            $UsersModel = new Users();
            $Users = $UsersModel->GetUserByEmail($Email);
            $UsersModel->UpdateExpiryDate($Users->ID, $ExpiryDate,$Password);
            
            //Sending Email
            if($Param):
                //$UserNames = session()->get('UserName');
                Mail::send('auth.emails.ResetPassword', ['Password' => $Password], function ($m) use ($Users) {
                    $m->from('COBS@psopk.com', 'Administrator');
                    $m->to($Users->Email, $Users->UserName)->subject('EFISystem Reset Password');
                });
                Mail::send('auth.emails.AdminResetPassword', ['Users'=> $Users,'Password' => $Password], function ($m) use ($Users) {
                    $m->from('COBS@psopk.com', 'Administrator');
                    $m->to('superadmin@mailinator.com', 'Super Admin')->subject('EFISystem Reset Password');
                });
            endif;

            $request->flash();
            if ($Param):
                if ($Users->UserType == "admin"):
                    return redirect('/adminlist');
                elseif ($Users->UserType == "superuser"):
                    return redirect('/superuserlist');
                elseif ($Users->UserType == "contractor"):
                    return redirect('/contractorlist');
                endif;
            else:
                return redirect('' . '?reset=1');
            endif;

        endif;
    }

    public function checkemail() { 
        //Checking Logged in user
//        $UserID = session()->get('ID');
//        if ($UserID == null):
//            return redirect('');
//        endif;

        $Email = $_POST['Email'];
        $UsersModel = new Users();
        $Users = $UsersModel->GetUserByEmail($Email);
        if ($Users):
            return 1;
        else:
            return 0;
        endif;
        exit;
    }

    public function checkusername() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        if ($UserID == null):
            return redirect('');
        endif;

        $UserName = $_POST['UserName'];
        $UsersModel = new Users();
        $Users = $UsersModel->GetUserByUserName($UserName);
        if ($Users):
            return 1;
        else:
            return 0;
        endif;
        exit;
    }

    public function CheckVendorCode() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        if ($UserID == null):
            return redirect('');
        endif;

        $VendorCode = $_POST['VendorCode'];
        $UsersModel = new Users();
        $Users = $UsersModel->GetUserByVendorCode($VendorCode);
        if ($Users):
            return 1;
        else:
            return 0;
        endif;
        exit;
    }

    public function adminlist() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType == 'admin' || $UserType == 'superuser' || $UserType == 'contractor'):
            return redirect('');
        endif;

        $UsersModel = new Users();
        $List = $UsersModel->getAll('admin');
        return view('adminlist', ['List' => $List]);
    }

    public function superuserlist() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType == 'superuser' || $UserType == 'contractor'):
            return redirect('');
        endif;

        $UsersModel = new Users();
        $List = $UsersModel->getAll('superuser');
        return view('superuserlist', ['List' => $List]);
    }

    public function contractorlist() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType == 'contractor'):
            return redirect('');
        endif;

        $UsersModel = new Users();
        $List = $UsersModel->getAll('contractor');
        return view('contractorlist', ['List' => $List]);
    }

    public function invoices() { 
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType != 'contractor'):
            return redirect('');
        endif;

        $UserName = session()->get('UserName');
        $List = "";
        $Param = $_SERVER['QUERY_STRING'];
        if ($Param):
            $InvoiceModel = new Efiinvoice();
            $List = $InvoiceModel->GetInvoiceByName($Param);
            $Param = 1;
        endif;

        $UsersModel = new Users();
        $contractorsList = $UsersModel->getAll('contractor');
        $Row = $UsersModel->GetUserByUserName($UserName);
        $VendorCode = $Row->VendorCode;

        $flag = false;
        $filename = "";
        $FetchedInvoices = "";
        $filedetail = "";
        $fDate = "";
        $tDate = "";

        $files = array_diff(scandir(storage_path() . '/soapfiles/'), array('.', '..'));
        
        foreach ($files as $key => $value) {
            $UID = explode("_", $value);
            
            if ($UID[0] == $UserID) {
                $filename = $value;

                $filedetail = explode("_", $filename);
                $fDate = '"' . $filedetail[1] . '"';

                $tDate = $filedetail[2];
                $tDate = '"' . substr($tDate, 0, strrpos($tDate, '.')) . '"';
                $flag = true;
            }
        }
       
        if ($flag) {
            try {
                $Path = storage_path() . '/soapfiles/' . $filename;
                $FetchedInvoices = File::get($Path);
            } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
                die("The file doesn't exist");
            }
        }
        
        return view('invoices', ['List' => $List, 'Param' => $Param, 'contractorsList' => $contractorsList, 'UserType' => $UserType, 'UserName' => $UserName, 'UserID' => $UserID, 'VendorCode' => $VendorCode, 'FetchedInvoices' => $FetchedInvoices, 'fDate' => $fDate, 'tDate' => $tDate, 'filename' => $filename]);
    }

    public function getInvoice() {
        $wsdl = "http://172.16.4.167/efisys/storage/MI_MM_INVOICE_ACK.wsdl";
        $paramArray = array("login" => "pisuper",
            "password" => "lotus123",
            "CLIENT" => "500",
            'cache_wsdl' => WSDL_CACHE_NONE);

        $client = new SoapClient($wsdl, $paramArray);

        echo 'Client:';
        echo '<pre>';
        print_r($client);
        echo '</pre>';

        $parameters = array("ACTTYPE" => "SR",
            "VENDOR" => "7773000652",
            "DATEFROM" => "01.08.2015",
            "DATETO" => "30.09.2015");

        try {
            $response = $client->MI_MM_INVOICE_ACK($parameters);
            echo 'Calling Function:';
            echo '<pre>';
            print_r($response);
            echo '</pre>';
            exit;
        } catch (SoapFault $fault) {
            echo '<pre>';
            print_r($fault);
            echo '</pre>';
            exit;
        }
    }

    public function fetchinvoice() { 
        ini_set('max_exeuution_time', 3000);
        set_time_limit(3000);
        ini_set('default_socket_timeout', 3000);

        Log::useDailyFiles(storage_path() . '/logs/Laravel.log');
        Log::info('*****Fetching Invoice*****');

        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType != 'contractor'):
            return redirect('');
        endif;

        $UserName = session()->get('UserName');
        $List = "";
        $Fetch = $_POST['Fetch'];
        $FromDate = $_POST['fromDate'];
        $ToDate = $_POST['toDate'];
        $VENDOR = $_POST['contractorId'];
        $ShipmentNo = $_POST['shipmentNo'];

        $FromDate = strtotime($FromDate);
        $FromDate = date('d.m.Y', $FromDate);
        $ToDate = strtotime($ToDate);
        $ToDate = date('d.m.Y', $ToDate);

        //WSDL File
        $wsdl = "http://172.16.4.167/efisys/storage/MI_MM_INVOICE_ACK.wsdl";

        Log::info('WSDL URL=' . $wsdl);

        //Login Cardentials
        $paramArray = array("login" => "pisuper",
            "password" => "lotus123",
            "CLIENT" => "500",
            'cache_wsdl' => WSDL_CACHE_NONE);

        Log::info('Login Array=' . print_r($paramArray, true));

        //Connetion To Web Service
        $client = new SoapClient($wsdl, $paramArray);

        Log::info('Client=' . print_r($client, true));

        //Input To Function
        $parameters = array("ACTTYPE" => "SR",
            "VENDOR" => $VENDOR,
            "DATEFROM" => $FromDate,
            "DATETO" => $ToDate);


        Log::info('Paramaters=' . print_r($parameters, true));
        try {
            //Array For Testing
//            $paramObj = new stdClass;
//            $paramObj->RETURN = new stdClass;
//            $paramObj->RETURN->TYPE = "";
//            $paramObj->RETURN->CODE = "";
//            $paramObj->RETURN->MESSAGE = "0";
//            $paramObj->RETURN->LOG_NO = "";
//            $paramObj->RETURN->LOG_MSG_NO = "000000";
//            $paramObj->RETURN->MESSAGE_V1 = "";
//            $paramObj->RETURN->MESSAGE_V2 = "";
//            $paramObj->RETURN->MESSAGE_V3 = "";
//            $paramObj->RETURN->MESSAGE_V4 = "";
//            $paramObj->SAPINVOICE = "48";
//            $paramObj->YEAR = "2016";
//            $paramObj->INVDATA = new stdClass;
//            $paramObj->INVDATA->item = array(
//            0 => new stdClass,
//            1 => new stdClass,
//            2 => new stdClass,
//            3 => new stdClass,
//            );
//            $paramObj->INVDATA->item[0]->LIFNR = "7773000652";
//            $paramObj->INVDATA->item[0]->NAME1 = "MUSLIM CARRIAGE";
//            $paramObj->INVDATA->item[0]->SHNUMBER = "8217559";
//            $paramObj->INVDATA->item[0]->DELDATE = "28.06.2016";
//            $paramObj->INVDATA->item[0]->VBELN = "0886168964";
//            $paramObj->INVDATA->item[0]->VEHICLE = "UNKPRK7040";
//            $paramObj->INVDATA->item[0]->AMOUNT = "28251.50";
//            $paramObj->INVDATA->item[0]->CURR = "";
//            $paramObj->INVDATA->item[0]->QTY = "25000";
//            $paramObj->INVDATA->item[0]->UNIT = "L";
//            $paramObj->INVDATA->item[0]->ACK = "";
//            $paramObj->INVDATA->item[0]->F1 = "001297";
//            $paramObj->INVDATA->item[0]->F2 = "HSD";
//            $paramObj->INVDATA->item[0]->F3 = "UNKPRK7040";
//            $paramObj->INVDATA->item[0]->F4 = "H. NO. Z-695 BABU LAL HUSSAIN";
//            $paramObj->INVDATA->item[0]->F5 = "2706839-0";
//            $paramObj->INVDATA->item[0]->RBLGP = "001297";
//
//            $paramObj->INVDATA->item[1]->LIFNR = "7773000652";
//            $paramObj->INVDATA->item[1]->NAME1 = "MUSLIM CARRIAGE";
//            $paramObj->INVDATA->item[1]->SHNUMBER = "8217581";
//            $paramObj->INVDATA->item[1]->DELDATE = "28.06.2016";
//            $paramObj->INVDATA->item[1]->VBELN = "0886169659";
//            $paramObj->INVDATA->item[1]->VEHICLE = "UNKGLTA666";
//            $paramObj->INVDATA->item[1]->AMOUNT = "45202.40";
//            $paramObj->INVDATA->item[1]->CURR = "";
//            $paramObj->INVDATA->item[1]->QTY = "40000";
//            $paramObj->INVDATA->item[1]->UNIT = "L";
//            $paramObj->INVDATA->item[1]->ACK = "";
//            $paramObj->INVDATA->item[1]->F1 = "001316";
//            $paramObj->INVDATA->item[1]->F2 = "HSD";
//            $paramObj->INVDATA->item[1]->F3 = "UNKGLTA6667";
//            $paramObj->INVDATA->item[1]->F4 = "H. NO. Z-695 BABU LAL HUSSAIN";
//            $paramObj->INVDATA->item[1]->F5 = "2706839-0";
//            $paramObj->INVDATA->item[1]->RBLGP = "001316";
//
//            $paramObj->INVDATA->item[2]->LIFNR = "7773000652";
//            $paramObj->INVDATA->item[2]->NAME1 = "MUSLIM CARRIAGE";
//            $paramObj->INVDATA->item[2]->SHNUMBER = "123456";
//            $paramObj->INVDATA->item[2]->DELDATE = "28.06.2016";
//            $paramObj->INVDATA->item[2]->VBELN = "0886169659";
//            $paramObj->INVDATA->item[2]->VEHICLE = "UNKGLTA666";
//            $paramObj->INVDATA->item[2]->AMOUNT = "45202.40";
//            $paramObj->INVDATA->item[2]->CURR = "";
//            $paramObj->INVDATA->item[2]->QTY = "40000";
//            $paramObj->INVDATA->item[2]->UNIT = "L";
//            $paramObj->INVDATA->item[2]->ACK = "";
//            $paramObj->INVDATA->item[2]->F1 = "001316";
//            $paramObj->INVDATA->item[2]->F2 = "HSD";
//            $paramObj->INVDATA->item[2]->F3 = "UNKGLTA6667";
//            $paramObj->INVDATA->item[2]->F4 = "H. NO. Z-695 BABU LAL HUSSAIN";
//            $paramObj->INVDATA->item[2]->F5 = "2706839-0";
//            $paramObj->INVDATA->item[2]->RBLGP = "001316";
//            
//            $paramObj->INVDATA->item[3]->LIFNR = "77730006523";
//            $paramObj->INVDATA->item[3]->NAME1 = "MUSLIM CARRIAGE";
//            $paramObj->INVDATA->item[3]->SHNUMBER = "123";
//            $paramObj->INVDATA->item[3]->DELDATE = "28.06.2016";
//            $paramObj->INVDATA->item[3]->VBELN = "0886169659";
//            $paramObj->INVDATA->item[3]->VEHICLE = "UNKGLTA666";
//            $paramObj->INVDATA->item[3]->AMOUNT = "45202.40";
//            $paramObj->INVDATA->item[3]->CURR = "";
//            $paramObj->INVDATA->item[3]->QTY = "40000";
//            $paramObj->INVDATA->item[3]->UNIT = "L";
//            $paramObj->INVDATA->item[3]->ACK = "";
//            $paramObj->INVDATA->item[3]->F1 = "001316";
//            $paramObj->INVDATA->item[3]->F2 = "HSD";
//            $paramObj->INVDATA->item[3]->F3 = "UNKGLTA6667";
//            $paramObj->INVDATA->item[3]->F4 = "H. NO. Z-695 BABU LAL HUSSAIN";
//            $paramObj->INVDATA->item[3]->F5 = "2706839-0";
//            $paramObj->INVDATA->item[3]->RBLGP = "001316";
//            
//            $response = $paramObj;
            
            // Function Call // uncomment while uploading on server
            $response = $client->MI_MM_INVOICE_ACK($parameters);

            //Save File Code.
            $filename = $UserID . '_' . $FromDate . '_' . $ToDate;
            $path = storage_path() . '/soapfiles/' . $filename . '.json';
            $flag = false;
            $file = "";

            $files = array_diff(scandir(storage_path() . '/soapfiles/'), array('.', '..'));
            foreach ($files as $key => $value) {
                
                $UID = explode("_", $value);
                if ($UID[0] == $UserID) {
                    $file = $value;
                    $flag = true;
                }
            }

            if ($flag) { 
                if (File::exists($path)) {
                    //Checking File With Same Date
                    //"file already exist of selected date"."<br>";

                    try {
                        $Path = storage_path() . '/soapfiles/' . $filename . '.json';
                        $response = File::get($Path);
                        //echo $contents;
                    } catch (Illuminate\Filesystem\FileNotFoundException $exception) {
                        die("The file doesn't exist");
                    }
                } else { 
                    //Checking File If Date Has Changed
                    //One user have only one file so delete old one and create new file.
                    $delPath = storage_path() . '/soapfiles/' . $file;
                    File::delete($delPath);
                    if(isset($response->INVDATA->item)):
                        $response = $response->INVDATA->item;
                        foreach($response as $key => $ValResponse):
                            $response[$key]->Status = "0";
                        endforeach;
                    else:
                        $response = "";
                    endif;
                    //$response = $response->INVDATA->item;

                    $data = json_encode($response);
                    $bytes_written = File::put($path, $data);
                    if ($bytes_written === false) {
                        die("Error writing to file");
                    }
                    $response = json_encode($response);
                }
            } else { 
                //"create first file of current user"."<br>"; 
                // Function Call // uncomment while uploading on server
                //   $response = $client->MI_MM_INVOICE_ACK($parameters);
                if(isset($response->INVDATA->item)):
                    $response = $response->INVDATA->item;
                    foreach($response as $key => $ValResponse):
                        $response[$key]->Status = "0";
                    endforeach;
                else:
                    $response = "";
                endif;
                //$response = $response->INVDATA->item;
                $data = json_encode($response);
                $bytes_written = File::put($path, $data);
                if ($bytes_written === false) {
                    die("Error writing to file");
                }

                $response = json_encode($response);
            }

//            if(isset($response->INVDATA->item)):
//                $response = $response->INVDATA->item;
//            else:
//                $response = "";
//            endif;
            Log::info('Response=' . print_r($response, true));
        } catch (SoapFault $fault) {
            Log::info('Fault=' . print_r($fault->faultstring, true));
            $fault = $fault->faultstring;
            if ($fault == "Could not connect to host"):
                $response = "1";
            elseif ($fault == "Error Fetching http headers"):
                $response = "2";
            elseif ($fault == "Server Error"):
                $response = "3";
            elseif ($fault == "Service Unavailable"):
                $response = "4";
            endif;
        }
        
        echo $response;
        exit;
    }

    public function fetchinvoicedetail() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType != 'contractor'):
            return redirect('');
        endif;

        $List = "";
        $InvoiceID = $_POST['id'];
        if ($InvoiceID):
            $InvoiceModel = new Efiinvoice();
            $List = $InvoiceModel->GetInvoiceByID($InvoiceID);
        endif;
        echo json_encode($List);
        exit;
    }

    public function SubmitInvoice() { 
        ini_set('max_exeuution_time', 3000);
        set_time_limit(3000);
        ini_set('default_socket_timeout', 3000);
        
        Log::useDailyFiles(storage_path() . '/logs/Laravel.log');
        Log::info('*****Submitting Invoice*****');

        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        $UserName = session()->get('UserName');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType != 'contractor'):
            return redirect('');
        endif;

        //Intializing Model
        $SubmittedInvoiceModel = new SubmittedInvoice();
        $EfiModel = new Efiinvoice();

        $ShipmentNo = $_POST['ShipmentNo'];

        $FromDate = $_POST['FromDate'];
        $FromDate = strtotime($FromDate);
        $FromDate = date('d.m.Y', $FromDate);

        $ToDate = $_POST['ToDate'];
        $ToDate = strtotime($ToDate);
        $ToDate = date('d.m.Y', $ToDate);

        $RandomNum = rand();


        //WSDL File
        $wsdl = "http://172.16.4.167/efisys/storage/MI_MM_INVOICE_ACK.wsdl";

        Log::info('WSDL Url='.$wsdl);
        //Login Cardentials
        $paramArray = array("login" => "pisuper",
            "password" => "lotus123",
            "CLIENT" => "500",
            'cache_wsdl' => WSDL_CACHE_NONE);

        Log::info('Login Array=' . print_r($paramArray, true));

        //Connetion To Web Service
        $client = new SoapClient($wsdl, $paramArray);

        Log::info('Client=' . print_r($client, true));

        //Array Size
        $ArraySize = sizeof($ShipmentNo);

        //parameter array for calling function
        $parameters = new stdclass;
        $parameters->ACTTYPE = "PO";
        $parameters->VENDOR = $ShipmentNo[0][1];
        $parameters->DATEFROM = $FromDate;
        $parameters->DATETO = $ToDate;
        $parameters->INVDATA = new stdclass;
        $parameters->INVDATA->item = array(
        );
        for ($i = 0; $i < $ArraySize; $i++):
            array_push($parameters->INVDATA->item, (object) $i);
            unset($parameters->INVDATA->item[$i]->scalar);
        endfor;

        foreach ($ShipmentNo as $key => $ValShipmentNo):

            $ShipmentNum = $ValShipmentNo[0];
            $LIFNR = $ValShipmentNo[1];
            $NAME1 = $ValShipmentNo[2];
            $SHNUMBER = $ValShipmentNo[3];
            $DELDATE = $ValShipmentNo[4];
            $VBELN = $ValShipmentNo[5];
            $VEHICLE = $ValShipmentNo[6];
            $AMOUNT = $ValShipmentNo[7];
            $CURR = "";
            $QTY = $ValShipmentNo[8];
            $UNIT = $ValShipmentNo[9];
            $ACK = $ValShipmentNo[10];
            $F1 = $ValShipmentNo[11];
            $F2 = $ValShipmentNo[12];
            $F3 = $ValShipmentNo[13];
            $F4 = $ValShipmentNo[14];
            $F5 = $ValShipmentNo[15];
            $RBLGP = $ValShipmentNo[16];

            //Inserting Invoice in DB
            $EfiModel->InsertInvoice($LIFNR, $NAME1, $SHNUMBER, $DELDATE, $VBELN, $VEHICLE, $AMOUNT, $CURR, $QTY, $UNIT, $ACK, $F1, $F2, $F3, $F4, $F5, $RBLGP, $RandomNum);

            $parameters->INVDATA->item[$key]->LIFNR = $LIFNR;
            $parameters->INVDATA->item[$key]->NAME1 = $NAME1;
            $parameters->INVDATA->item[$key]->SHNUMBER = $SHNUMBER;
            $parameters->INVDATA->item[$key]->DELDATE = $DELDATE;
            $parameters->INVDATA->item[$key]->VBELN = $VBELN;
            $parameters->INVDATA->item[$key]->VEHICLE = $VEHICLE;
            $parameters->INVDATA->item[$key]->AMOUNT = $AMOUNT;
            $parameters->INVDATA->item[$key]->CURR = $CURR;
            $parameters->INVDATA->item[$key]->QTY = $QTY;
            $parameters->INVDATA->item[$key]->UNIT = $UNIT;
            $parameters->INVDATA->item[$key]->ACK = "X";
            $parameters->INVDATA->item[$key]->F1 = $F1;
            $parameters->INVDATA->item[$key]->F2 = $F2;
            $parameters->INVDATA->item[$key]->F3 = $F3;
            $parameters->INVDATA->item[$key]->F4 = $F4;
            $parameters->INVDATA->item[$key]->F5 = $F5;
            $parameters->INVDATA->item[$key]->RBLGP = $RBLGP;

        endforeach;

        Log::info('Parameters Array=' . print_r($parameters, true));

        // Function Call // uncomment while uploading on server
        try {
            $response = $client->MI_MM_INVOICE_ACK($parameters);
            Log::info('Response=' . print_r($response, true));
        } catch (SoapFault $fault) {
            Log::info('Fault=' . print_r($fault->faultstring, true));
        }
        
        if (isset($response->SAPINVOICE)):
            $SAPINVOICE = $response->SAPINVOICE;
        else:
            $SAPINVOICE = "1";
        endif;

        //$SAPINVOICE = 123;
        Log::info('SapInvoice=' . $SAPINVOICE);

        //Inserting Data To DB
        if ($SAPINVOICE != "1" && $SAPINVOICE != "") {
            //Track For User Who is submitting invoice
            $Time = time() + 18000;
            $Description = $UserName."(".$_SERVER['REMOTE_ADDR'].") submitted invoice having Sap Invoice:".$SAPINVOICE." at ".date("Y-m-d H:i:s", $Time);
            $LogModel = new Logs();
            $LogModel->InsertLogs($UserName,$UserName,date("Y-m-d H:i:s", $Time),$Description);
            
            $SubmittedInvoiceModel->InsertSubmittedInvoice($UserName, $ShipmentNum, $SAPINVOICE);
            $EfiModel->UpdateInvoice($RandomNum, $SAPINVOICE);
        }

        echo $SAPINVOICE;
        exit();
    }

    public function submittedinvoices() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        $UserName = session()->get('UserName');
        if ($UserID == null):
            return redirect('');
        endif;

        //Initialize Model
        $UsersModel = new Users();
        $EfiInvoiceModel = new Efiinvoice();
        $User = $UsersModel->GetUserByUserID($UserID);
        $NewList = array();
        
        $SubmittedInvoiceModel = new SubmittedInvoice();
        if ($UserType == 'contractor'):
            $List = $SubmittedInvoiceModel->GetInvoiceByContractorID($UserName);
            foreach($List as $key => $ValList):
                if($ValList->SAPINVOICE):
                    $Rows = $EfiInvoiceModel->GetSHNUMBySAPNUM($ValList->SAPINVOICE);
                    $Rows = implode(" ",$Rows);
                    $List[$key]->SHNUMBER = $Rows;
                endif;
            endforeach;
        else:
            $List = $SubmittedInvoiceModel->GetAllInvoice();
            foreach($List as $key => $ValList):
                if($ValList->SAPINVOICE):
                    $Rows = $EfiInvoiceModel->GetSHNUMBySAPNUM($ValList->SAPINVOICE);
                    $Rows = implode(" ",$Rows);
                    $List[$key]->SHNUMBER = $Rows;
                endif;
            endforeach;
        endif;
        return view('submittedinvoices', ['List' => $List, 'UserType' => $UserType,'UserDetail' => $User]);
    }

    public function cancelsubmittedinvoice() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        if ($UserType == 'contractor'):
            return redirect('');
        endif;

        $ShipmentID = $_POST['ShipmentID'];
        $ShipmentNo = $_POST['ShipmentNo'];
        $CancelNumber = $_POST['CancelNumber'];
        $CancelDate = $_POST['CancelDate'];

        //Initilize Model
        $SubmittedInvoiceModel = new SubmittedInvoice();
        $newArray = array();
        foreach ($ShipmentID as $key => $value):
            $newArray[$key]['ShipmentID'] = $ShipmentID[$key];
            $newArray[$key]['ShipmentNo'] = $ShipmentNo[$key];
            $newArray[$key]['CancelNumber'] = $CancelNumber[$key];
            $newArray[$key]['CancelDate'] = date('Y-m-d', strtotime($CancelDate[$key]));
        endforeach;

        foreach ($newArray as $newVal):
            $SubmittedInvoiceModel->UpdateSubmittedInvoice($newVal['ShipmentID'], $newVal['ShipmentNo'], $newVal['CancelNumber'], $newVal['CancelDate']);
        endforeach;

        exit();
    }

    public function printinvoice() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        $UserName = session()->get('UserName');
        if ($UserID == null):
            return redirect('');
        endif;

        //Intializing Model
        $UserModel = new Users();
        $Detail = $UserModel->GetUserByUserID($UserID);

        $thelist = array();

        if ($UserType == 'contractor') {
            if ($handle = opendir('./storage/efifiles')) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != ".." && $file == $Detail->VendorCode) {
                        array_push($thelist, $file);
                    }
                }
                closedir($handle);
            }
        } else {
            if ($handle = opendir('./storage/efifiles')) {
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        array_push($thelist, $file);
                    }
                }
                closedir($handle);
            }
        }

        return view('printinvoice', ['directories' => $thelist]);
    }

    public function fetchpdfinvoices() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;

        $List = "";
        $folderName = $_POST['folderName'];
        if ($folderName):
            if ($handle = opendir('./storage/efifiles/' . $folderName)) {
                $files=array();
                while (false !== ($file = readdir($handle))) {
                    if ($file != "." && $file != "..") {
                        $files[filemtime('./storage/efifiles/' . $folderName."/".$file)] = $file;
                    }
                }
                closedir($handle);
                krsort($files);
                foreach($files as $file){
                    $List .= '<li><a href="./storage/efifiles/' . $folderName . '/' . $file . '" target="_blank" title="Click To Download">' . $file . '</a></li>';
                }
            }
        endif;
        echo json_encode($List);
        exit;
    }

    public function SubmittedInvoiceDetail() {
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;

        $SAPINVOICE = $_POST['SAPINVOICE'];

        //Intilizing Model
        $EfiInvoiceModel = new Efiinvoice();
        $Rows = $EfiInvoiceModel->GetInvoiceBySAPNUM($SAPINVOICE);

        echo json_encode($Rows);
        exit;
    }
    
    public function UpdateJSON(){ 
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        
        //Post Variables
        $StoredData = $_POST['StoredData'];
        $FileName = $_POST['FileName'];
        
        //path of json file
        $Path = storage_path() . '/soapfiles/' . $FileName;
        
        //encode Data
        if($StoredData):
            $Data = json_encode($StoredData);
        else:
            $Data = "";
        endif;
        
        //write on file
        $bytes_written = File::put($Path, $Data);
        
        //incase of error
        if ($bytes_written === false) {
            die("Error writing to file");
        }
        
        exit;
        
    }
    
    public function EditContractor(){
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        
        //Post Variables
        $ContractorID = $_POST['ContractorID'];
        
        //Initializing Model
        $UserModel = new Users();
        $Row = $UserModel->GetUserByUserID($ContractorID);
        
        echo json_encode($Row);
        exit;
        
    }
    
    public function UpdateContractor(){
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        if ($UserID == null):
            return redirect('');
        endif;
        
        //Post Variables
        $Address = $_POST['Address'];
        $ContactNo = $_POST['ContactNo'];
        $ContractorID = $_POST['ContractorID'];
        
        //Initializing Model
        $UserModel = new Users();
        $UserModel->UpdateContractor($ContractorID, $Address, $ContactNo);
        
        exit();
    }
    
    public function DisableUser(){
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        $UserName = session()->get('UserName');
        if ($UserID == null):
            return redirect('');
        endif;
        
        //Post Variables
        $ID = $_POST['ID'];
        $Time = time() + 18000;
        //Initializing Model
        $UserModel = new Users();
        $Row = $UserModel->GetUserByUserID($ID);
        $UserModel->DisableUser($ID);
        
        $Description = $UserName."(".$_SERVER['REMOTE_ADDR'].") Disabled ".$Row->UserName." at ".date("Y-m-d H:i:s", $Time);
        $LogModel = new Logs();
        $LogModel->InsertLogs($UserName,$Row->UserName,date("Y-m-d H:i:s", $Time),$Description);
        exit;
    }
    
    public function EnableUser(){
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        $UserName = session()->get('UserName');
        if ($UserID == null):
            return redirect('');
        endif;
        
        //Post Variables
        $ID = $_POST['ID'];
        $Time = time() + 18000;
        
        //Initializing Model
        $UserModel = new Users();
        $Row = $UserModel->GetUserByUserID($ID);
        $UserModel->EnableUser($ID);
        
        $Description = $UserName."(".$_SERVER['REMOTE_ADDR'].") Enabled ".$Row->UserName." at ".date("Y-m-d H:i:s", $Time);;
        $LogModel = new Logs();
        $LogModel->InsertLogs($UserName,$Row->UserName,date("Y-m-d H:i:s", $Time),$Description);
        
        exit;
    }
    
    public function Logs(){
        //Checking Logged in user
        $UserID = session()->get('ID');
        if ($UserID == null):
            return redirect('');
        endif;
        
        
        $LogModel = new Logs();
        $List = $LogModel->getAll();        
        
        return view('Logs', ['List' => $List]);
    }
 
    public function ForgotPassword(Request $request){
        Log::useDailyFiles(storage_path() . '/logs/Laravel.log');
        Log::info('*****ForgotPassword*****');
        ini_set('xdebug.max_nesting_level', 2000);
        //checking Form Post
        $Msg = "";
        $Msg1 = "";
        $get_params = $_SERVER['QUERY_STRING'];
        $ID = explode("=",$get_params);
        
        if ($request->input()):
            $Email = $request->input('email');
        
            $UsersModel = new Users();
            $Row = $UsersModel->GetUserByEmail($Email);
            $RandomNumber = rand(0,100000); 
            if(count($Row) > 0):
                $user = $Row->UserName;
                $Url = URL::to('/ChangePassword?ID='.$ID[1].'&Token='.$RandomNumber);
                //$Url = 'http://172.16.4.167/efisys/ChangePassword?ID='.$ID[1].'&Token='.$RandomNumber;
                Log::info('*****Sending Email*****');
                Mail::send('auth.emails.ForgotPassword', ['Url' => $Url], function ($m) use ($user,$Row,$Url) {
                    $m->from('COBS@psopk.com', 'Administrator');

                    $m->to($Row->Email, $Row->UserName)->subject('EFISystem Forgot Password');
                });
                Log::info('*****Email Sent*****');
                $Msg = "Email Has Been Sent";
                $UsersModel->UpdateTokenByID($Row->ID, $RandomNumber);
            else:
                $Msg1 = "PLease Enter Correct Email";
            endif;
            
            return view('ForgotPassword',['Msg'=>$Msg,'Msg1'=>$Msg1,'ID'=>$ID[1]]);

        endif;
        
    }
    
    public function ChangePassword(Request $request){
        //Get Parameters
        $get_params = $_SERVER['QUERY_STRING'];
        $get_params = explode("&",$get_params);
        $ID = explode("=",$get_params[0]);
        $Email = explode("=",$get_params[1]);
       
        $Msg = "";
        $Msg1 = "";
        //checking Form Post
        if ($request->input()):
            $OldPassword = $request->input('oldpassword');
            $NewPassword = $request->input('newpassword');
            $ConfPassword = $request->input('confpassword');
            
            $UsersModel = new Users();
            $Row = $UsersModel->GetUserByToken($Email[1]);
            if($ID[1] == 2):
                if($OldPassword == $Row->Password):
                    if(count($Row) > 0):

                            if($NewPassword == $ConfPassword):
                                $ExpiryDate = date("Y-m-d", (time() + 2.592e+6));
                                $UsersModel->UpdateExpiryDate($Row->ID, $ExpiryDate,$NewPassword);
                                $Msg = "Password Has Been changed Successfully.Please Login To Continue";
                            else:
                                $Msg1 = "New Password Does Not Match With Confirm Password";
                            endif;

                    else:
                        $Msg1 = "Please Enter Valid Password";
                    endif;
                else:
                    $Msg1 = "Please Enter Correct Old Password";
                endif;
            elseif($ID[1] == 1): 
                if(count($Row) > 0):
                        if($NewPassword == $ConfPassword):
                            $ExpiryDate = date("Y-m-d", (time() + 2.592e+6));
                            $UsersModel->UpdateExpiryDate($Row->ID, $ExpiryDate,$NewPassword);
                            $Msg = "Password Has Been changed Successfully.Please Login To Continue";
                        else:
                            $Msg1 = "New Password Does Not Match With Confirm Password";
                        endif;

                else:
                    $Msg1 = "Please Enter Valid Password";
                    endif;
            endif;
            
            return redirect('' . '?ChangePassword=2');
            //return view('ChangePassword',['Msg'=>$Msg, 'Msg1' => $Msg1,'Email'=>$Email[1]]);

        endif;
    }
    
    public function UpdateLoginTime(){
        //Checking Logged in user
        $UserID = session()->get('ID');
        $UserType = session()->get('Name');
        $UserName = session()->get('UserName');
        if ($UserType == 'admin' || $UserType == 'superuser' || $UserType == 'contractor'):
            return redirect('');
        endif;
        
        $UserModel = new Users();
        $UserModel->UpdateLoginTime($UserID);
        exit;
    }

}
