-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 24, 2016 at 11:17 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `efisystem`
--

-- --------------------------------------------------------

--
-- Table structure for table `efiinvoice`
--

CREATE TABLE IF NOT EXISTS `efiinvoice` (
  `ID` int(25) NOT NULL AUTO_INCREMENT,
  `LIFNR` char(10) NOT NULL COMMENT 'Account Number Of Vendor And Creditor',
  `NAME1` char(30) NOT NULL COMMENT 'Name',
  `SHNUMBER` varchar(215) NOT NULL COMMENT 'Type of parameter field assignment for search help attachment',
  `DELDATE` varchar(25) NOT NULL COMMENT 'Delivery Date',
  `VBELN` char(10) NOT NULL COMMENT 'Delivery',
  `VEHICLE` char(10) NOT NULL COMMENT 'TD Vehicle Number',
  `AMOUNT` varchar(15) NOT NULL COMMENT 'Amount',
  `CURR` int(5) NOT NULL COMMENT 'Currency',
  `QTY` int(13) NOT NULL COMMENT 'Quantity',
  `UNIT` int(3) NOT NULL COMMENT 'Unit',
  `ACK` char(1) NOT NULL COMMENT 'Acknowledgment',
  `F1` char(30) NOT NULL COMMENT '30 Characters',
  `F2` char(30) NOT NULL COMMENT '30 Characters',
  `F3` char(30) NOT NULL COMMENT '30 Characters',
  `F4` char(30) NOT NULL COMMENT '30 Characters',
  `F5` char(50) NOT NULL COMMENT 'Comment',
  `RBLGP` varchar(25) NOT NULL,
  `SAPINVOICE` varchar(25) DEFAULT NULL,
  `RandomNum` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `efiinvoice`
--

INSERT INTO `efiinvoice` (`ID`, `LIFNR`, `NAME1`, `SHNUMBER`, `DELDATE`, `VBELN`, `VEHICLE`, `AMOUNT`, `CURR`, `QTY`, `UNIT`, `ACK`, `F1`, `F2`, `F3`, `F4`, `F5`, `RBLGP`, `SAPINVOICE`, `RandomNum`) VALUES
(1, 'abc', 'contractor1', 'a', '2016-06-23', 'abc', 'abc', '1000', 10, 10, 10, 'a', 'abc', 'abc', 'abc', 'abc', 'abc', '', NULL, NULL),
(2, 'abc', 'contractor2', 'a', '2016-06-24', 'abc', 'abc', '1000', 100, 100, 100, 'a', 'abc', 'abc', 'abc', 'abc', 'abc', '', NULL, NULL),
(3, 'abc', 'contractor1', 'a', '2016-06-25', 'abc', 'abc', '2000', 10, 10, 20, 'a', 'abc', 'abc', 'abc', 'abc', 'abc', '', NULL, NULL),
(4, '7773000586', 'AHMED BILAL TRADERS', '0', '0000-00-00', '0882084465', '0000004286', '2', 0, 0, 0, '', '000001', '', '', '', '', '000001', NULL, NULL),
(5, '7773000586', 'AHMED BILAL TRADERS', '0', '', '0882084465', '0000004286', '2', 0, 0, 0, '', '000001', '', '', '', '', '000001', NULL, NULL),
(6, '7773000586', 'AHMED BILAL TRADERS', '0005802357', '', '0882084465', '0000004286', '2', 0, 0, 0, '', '000001', '', '', '', '', '000001', NULL, NULL),
(7, '7773000586', 'AHMED BILAL TRADERS', '0005802357', '', '0882084465', '0000004286', '2', 0, 0, 0, '', '000001', '', '', '', '', '000001', NULL, NULL),
(8, '7773000586', 'AHMED BILAL TRADERS', '0005802914', '', '0882085375', '0000011774', '12', 0, 0, 0, '', '000002', '', '', '', '', '000002', NULL, NULL),
(9, '7773000586', 'AHMED BILAL TRADERS', '0005802914', '', '0882085375', '0000011774', '16', 0, 0, 0, '', '000002', '', '', '', '', '000002', NULL, NULL),
(10, '7773000586', 'AHMED BILAL TRADERS', '0005802914', '', '0882085375', '0000011774', '12', 0, 0, 0, '', '000002', '', '', '', '', '000002', NULL, '30828'),
(11, '7773000586', 'AHMED BILAL TRADERS', '0005802357', '', '0882085375', '0000011774', '16', 0, 0, 0, '', '000002', '', '', '', '', '000002', NULL, '30828'),
(12, '7773000586', 'AHMED BILAL TRADERS', '0005802914', '', '0882084465', '0000004286', '2', 0, 0, 0, '', '000001', '', '', '', '', '000001', '123', '3329'),
(13, '7773000586', 'AHMED BILAL TRADERS', '0005802357', '', '0882085375', '0000011774', '12', 0, 0, 0, '', '000002', '', '', '', '', '000002', '123', '3329'),
(14, '7773000586', 'AHMED BILAL TRADERS', '0005802914', '', '0882085375', '0000011774', '16', 0, 0, 0, '', '000002', '', '', '', '', '000002', '123', '3329'),
(15, '7773000652', 'MUSLIM CARRIAGE', '7460608', '22.07.2015', '0884919697', '0000024016', '105620', 0, 20000, 0, '', '000113', 'HSD', '', '', '', '000113', '5105653243', '21201'),
(16, '7773000652', 'MUSLIM CARRIAGE', '7464447', '24.07.2015', '0884926665', '0000024170', '79215', 0, 15000, 0, '', '000114', 'HSD', '', '', '', '000114', '5105653244', '21421'),
(17, '7773000652', 'MUSLIM CARRIAGE', '5801112', '01.06.2013', '0882081958', '0000016682', '117500', 0, 20000, 0, '', '000410', 'SKO', '', '', '', '000410', '', '29568'),
(18, '7773000652', 'MUSLIM CARRIAGE', '8217581', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '8193'),
(19, '7773000652', 'MUSLIM CARRIAGE', '123456', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '8193'),
(20, '7773000652', 'MUSLIM CARRIAGE', '8217581', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '7846'),
(21, '7773000652', 'MUSLIM CARRIAGE', '123456', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '7846'),
(22, '7773000652', 'MUSLIM CARRIAGE', '8217581', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '20711'),
(23, '7773000652', 'MUSLIM CARRIAGE', '123456', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '20711'),
(24, '7773000652', 'MUSLIM CARRIAGE', '8217581', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '4737'),
(25, '7773000652', 'MUSLIM CARRIAGE', '123456', '28.06.2016', '0886169659', 'UNKGLTA666', '45202', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', NULL, '4737'),
(31, '7773000652', 'MUSLIM CARRIAGE', '123456', '28.06.2016', '0886169659', 'UNKGLTA666', '45202.40', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', '123456789', '11976'),
(32, '7773000652', 'MUSLIM CARRIAGE', '8217581', '28.06.2016', '0886169659', 'UNKGLTA666', '45202.40', 0, 40000, 0, '', '001316', 'HSD', 'UNKGLTA6667', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001316', '123456789', '11976'),
(33, '7773000652', 'MUSLIM CARRIAGE', '8217559', '28.06.2016', '0886168964', 'UNKPRK7040', '28251.50', 0, 25000, 0, '', '001297', 'HSD', 'UNKPRK7040', 'H. NO. Z-695 BABU LAL HUSSAIN', '2706839-0', '001297', '123456789', '11976');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE IF NOT EXISTS `logs` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `Action_By` varchar(25) NOT NULL,
  `UserName` varchar(25) NOT NULL,
  `Time` datetime NOT NULL,
  `Description` varchar(500) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`ID`, `Action_By`, `UserName`, `Time`, `Description`) VALUES
(1, 'superadmin', 'contractor1', '2016-10-31 13:26:56', 'superadmin Disabled contractor1 at 2016-10-31 13:26:56'),
(2, 'superadmin', 'contractor1', '2016-10-31 13:26:59', 'superadmin Enabled contractor1 at 2016-10-31 13:26:59'),
(3, 'admin1', 'Contractor10', '2016-10-31 13:36:35', 'admin1 Disabled Contractor10 at 2016-10-31 13:36:35'),
(4, 'admin1', 'Contractor10', '2016-10-31 13:36:37', 'admin1 Enabled Contractor10 at 2016-10-31 13:36:37'),
(5, 'admin1', 'contractor3', '2016-10-31 13:36:43', 'admin1 Disabled contractor3 at 2016-10-31 13:36:43'),
(6, 'superuser1', 'contractor3', '2016-10-31 13:37:02', 'superuser1 Enabled contractor3 at 2016-10-31 13:37:02'),
(7, 'superuser1', 'contractor1', '2016-10-31 13:37:06', 'superuser1 Disabled contractor1 at 2016-10-31 13:37:06'),
(8, 'superadmin', 'contractor1', '2016-10-31 13:37:14', 'superadmin Enabled contractor1 at 2016-10-31 13:37:14'),
(9, 'superadmin', 'Contractor10', '2016-11-01 06:10:04', 'superadmin Disabled Contractor10 at 2016-11-01 06:10:04'),
(10, 'superadmin', 'Contractor10', '2016-11-01 06:10:12', 'superadmin Enabled Contractor10 at 2016-11-01 06:10:12'),
(11, 'superadmin', 'contractor1', '2016-11-07 10:46:18', 'superadmin Disabled contractor1 at 2016-11-07 10:46:18'),
(12, 'admin1', 'contractor1', '2016-11-07 11:07:27', 'admin1 Enabled contractor1 at 2016-11-07 11:07:27'),
(13, 'admin1', 'Contractor10', '2016-11-07 11:07:31', 'admin1 Disabled Contractor10 at 2016-11-07 11:07:31'),
(14, 'superadmin', 'contractor1', '2016-11-16 15:44:12', 'superadmin(::1) Disabled contractor1 at 2016-11-16 15:44:12'),
(15, 'superadmin', 'contractor1', '2016-11-16 15:44:31', 'superadmin(::1) Enabled contractor1 at 2016-11-16 15:44:31');

-- --------------------------------------------------------

--
-- Table structure for table `submitted_invoice`
--

CREATE TABLE IF NOT EXISTS `submitted_invoice` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(25) NOT NULL COMMENT 'Name of user',
  `SHNUMBER` varchar(25) NOT NULL COMMENT 'Shipment Number',
  `SAPINVOICE` varchar(25) DEFAULT NULL COMMENT 'Sap Invoice Number Soap Response',
  `CancelNumber` varchar(25) DEFAULT NULL COMMENT 'Cancel Number',
  `CancelDate` date DEFAULT NULL COMMENT 'Cancel Date',
  `IsCancel` int(1) NOT NULL DEFAULT '0' COMMENT '1-for cancel',
  PRIMARY KEY (`ID`),
  KEY `UserName` (`UserName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=91 ;

--
-- Dumping data for table `submitted_invoice`
--

INSERT INTO `submitted_invoice` (`ID`, `UserName`, `SHNUMBER`, `SAPINVOICE`, `CancelNumber`, `CancelDate`, `IsCancel`) VALUES
(1, 'superadmin', '5802357', '', NULL, NULL, 0),
(2, 'superadmin', '5802357', '', NULL, NULL, 1),
(3, 'superadmin', '5802357', '', '159', '2016-11-21', 1),
(4, 'superadmin', '5802914', '', '1', '2016-08-03', 1),
(5, 'superadmin', '5802914', '', '2', '2016-08-04', 1),
(8, 'contractor', '5802357', '', NULL, NULL, 0),
(9, 'contractor1', '5802357', '', '123456', '2016-08-22', 1),
(10, 'superadmin', '5802357', '', '0', '1970-01-01', 1),
(11, 'superadmin', '0', '', '123', '2016-08-10', 1),
(12, 'superadmin', '0', '', '159', '2016-08-08', 1),
(13, 'superadmin', '0', '', '1456', '2016-08-10', 1),
(14, 'superadmin', '0', '', '123', '2016-08-11', 1),
(15, 'superadmin', '0', '', '123', '2016-08-15', 1),
(16, 'superadmin', '123', '', '123', '2016-08-11', 1),
(17, 'superadmin', '123', '', NULL, NULL, 0),
(18, 'superadmin', '123', '', NULL, NULL, 0),
(19, 'superadmin', '123', '', NULL, NULL, 0),
(20, 'superadmin', '123', '', NULL, NULL, 0),
(21, 'superadmin', '123', '', NULL, NULL, 0),
(22, 'superadmin', '123', '', NULL, NULL, 0),
(23, 'superadmin', '123', '', NULL, NULL, 0),
(24, 'superadmin', '123', '', NULL, NULL, 0),
(25, 'superadmin', '123', '', NULL, NULL, 0),
(26, 'superadmin', '123', '', NULL, NULL, 0),
(27, 'superadmin', '123', '', NULL, NULL, 0),
(28, 'superadmin', '123', '', NULL, NULL, 0),
(29, 'superadmin', '123', '', NULL, NULL, 0),
(30, 'superadmin', '123', '', NULL, NULL, 0),
(31, 'superadmin', '123', '', NULL, NULL, 0),
(32, 'superadmin', '123', '', NULL, NULL, 0),
(33, 'superadmin', '123', '', NULL, NULL, 0),
(34, 'superadmin', '123', '', NULL, NULL, 0),
(35, 'superadmin', '123', '', NULL, NULL, 0),
(36, 'superadmin', '123', '', NULL, NULL, 0),
(37, 'superadmin', '0005802357', '123', NULL, NULL, 1),
(38, 'superadmin', '0005802914', '123', NULL, NULL, 1),
(39, 'superadmin', '0005802914', '123', NULL, NULL, 1),
(40, 'contractor1', '0005802914', '123', NULL, NULL, 0),
(41, 'contractor1', '0005802914', '123', NULL, NULL, 0),
(42, 'contractor1', '0005802357', '123', NULL, NULL, 0),
(43, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(44, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(45, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(46, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(47, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(48, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(49, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(50, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(51, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(52, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(53, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(54, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(55, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(56, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(57, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(58, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(59, 'superadmin', '0005802357', '123', NULL, NULL, 0),
(60, 'superadmin', '0005802357', '123', NULL, NULL, 0),
(61, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(62, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(63, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(64, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(65, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(66, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(67, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(68, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(69, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(70, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(71, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(72, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(73, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(74, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(75, 'superadmin', '0005802914', '123', NULL, NULL, 0),
(76, 'superadmin', '0005802914', '123', 'A-159', '1970-01-01', 1),
(77, 'superadmin', '0005802914', '', '123', '2016-08-22', 1),
(78, 'superadmin', '0005802914', '', '123', '2016-08-23', 1),
(79, 'contractor1', '0005802357', '123', NULL, NULL, 0),
(80, 'contractor1', '0005802357', '123', NULL, NULL, 0),
(81, 'contractor1', '0005802357', '123', NULL, NULL, 0),
(82, 'contractor1', '0005802914', '123', NULL, NULL, 0),
(83, 'contractor1', '0005802914', '123', NULL, NULL, 0),
(84, 'contractor1', '0005802914', '123', NULL, NULL, 0),
(85, 'contractor1', '7460608', '5105653243', NULL, NULL, 0),
(86, 'contractor1', '7464447', '5105653244', NULL, NULL, 0),
(87, 'contractor1', '5801112', '', NULL, NULL, 0),
(88, 'contractor1', '123456', '123456789', NULL, NULL, 0),
(89, 'contractor1', '123456', '123456789', NULL, NULL, 0),
(90, 'contractor1', '8217559', '123456789', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `ID` int(25) NOT NULL AUTO_INCREMENT COMMENT 'UserID',
  `UserName` varchar(100) NOT NULL COMMENT 'Full Name of User',
  `Password` varchar(25) NOT NULL,
  `VendorCode` varchar(25) DEFAULT NULL COMMENT 'Can be Null for power and administrator user, will mandatory in case of vendors/dealer',
  `UserType` varchar(25) NOT NULL COMMENT 'used for role assignment bifurcation',
  `Email` varchar(50) NOT NULL,
  `Address` varchar(100) DEFAULT NULL COMMENT 'Address Of Users',
  `ContactNo` varchar(25) NOT NULL,
  `Enable/Disable` int(1) NOT NULL COMMENT 'bit for enabling or disabling of user for portal 0->enable 1->disable',
  `ExpiryDate` date NOT NULL COMMENT 'expiry date for password change, reset for every month',
  `LoginTime` varchar(25) DEFAULT NULL COMMENT 'Login Time',
  `Token` varchar(25) DEFAULT NULL COMMENT 'Generate Token',
  PRIMARY KEY (`ID`),
  KEY `UserName` (`UserName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `UserName`, `Password`, `VendorCode`, `UserType`, `Email`, `Address`, `ContactNo`, `Enable/Disable`, `ExpiryDate`, `LoginTime`, `Token`) VALUES
(1, 'superadmin', '123456', '123456', 'superadmin', 'superadmin@mailinator.com', NULL, '12345678912', 0, '2016-09-14', '', NULL),
(2, 'admin1', '123456', '123456', 'admin', 'admin1@mailinator.com', NULL, '123456789123', 0, '2016-11-30', '', NULL),
(3, 'superuser1', '123456', '123456', 'superuser', 'superuser1@mailinator.com', NULL, '123456789123', 0, '2016-11-30', '1479800043', NULL),
(4, 'superuser2', '123456', '123456', 'superuser', 'superuser2@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(5, 'contractor1', '123456', '7773000652', 'contractor', 'contractor1@mailinator.com', 'H. NO. Z-695 BABU LAL HUSSAIN', '123456789123', 0, '2016-12-16', '', NULL),
(6, 'contractor2', '123456', '123456', 'contractor', 'contractor2@mailinator.com', 'H. NO. Z-695 BABU LAL HUSSAIN', '123456789123', 0, '2016-11-30', '', NULL),
(7, 'contractor3', '123456', '123456', 'contractor', 'contractor3@mailinator.com', 'H. NO. Z-695 BABU LAL HUSSAIN', '123456789123', 0, '2016-07-21', NULL, NULL),
(8, 'contractor4', '123456', '123456', 'contractor', 'contractor4@mailinator.com', 'H. NO. Z-695 BABU LAL HUSSAIN', '123456789123', 0, '2016-07-21', NULL, NULL),
(9, 'contractor5', '123456', '123456', 'contractor', 'contractor5@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(10, 'contractor6', '123456', '123456', 'contractor', 'contractor6@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(11, 'admin2', '123456', '123456', 'admin', 'admin2@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(12, 'admin3', '123456', '123456', 'admin', 'admin3@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(13, 'admin4', '123456', '123456', 'admin', 'admin4@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(14, 'admin5', '123456', '123456', 'admin', 'admin5@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(15, 'admin6', '123456', '123456', 'admin', 'admin6@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(16, 'admin7', '123456', '123456', 'admin', 'admin7@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(17, 'admin8', '123456', '123456', 'admin', 'admin8@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(18, 'admin9', '123456', '123456', 'admin', 'admin9@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(19, 'admin10', '123456', '123456', 'admin', 'admin10@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(20, 'admin11', '123456', '123456', 'admin', 'admin11@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(21, 'admin12', '123456', '123456', 'admin', 'admin12@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(22, 'admin13', '123456', '123456', 'admin', 'admin13@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(23, 'admin14', '123456', '123456', 'admin', 'admin14@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(24, 'admin15', '123456', '123456', 'admin', 'admin15@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(25, 'superuser3', '123456', '123456', 'superuser', 'superuser3@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(26, 'contractor7', '123456', '123456', 'contractor', 'contractor7@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(27, 'contractor8', '123456', '123456', 'contractor', 'contractor8@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(28, 'admin16', '123456', '123456', 'admin', 'admin16@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(29, 'admin17', '123456', '123456', 'admin', 'admin17@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(30, 'admin18', '123456', '123456', 'admin', 'admin18@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(31, 'admin19', '123456', '123456', 'admin', 'admin19@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(32, 'admin20', '123456', '123456', 'admin', 'admin20@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(33, 'admin21', '123456', '123456', 'admin', 'admin21@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(34, 'admin22', '123456', '123456', 'admin', 'admin22@mailinator.com', NULL, '123456789123', 0, '2016-07-21', NULL, NULL),
(35, 'superuser4', '123456', '123456', 'superuser', 'superuser4@mailinator.com', NULL, '123456', 0, '2016-07-21', NULL, NULL),
(36, 'superuser5', '123456', '123456789', 'superuser', 'superuser5@mailinator.com', NULL, '123456789', 0, '2016-10-01', NULL, NULL),
(37, 'superuser6', '123456', NULL, 'superuser', 'superuser6@mailinator.com', NULL, '123456789', 0, '2016-10-01', NULL, NULL),
(38, 'admin23', '123456', '', 'admin', 'admin23@mailinator.com', NULL, '123456', 0, '2016-10-01', NULL, NULL),
(39, 'Contractor10', '123456', '77730006523', 'contractor', 'contractor10@mailinator.com', 'H. NO. Z-695 BABU LAL HUSSAIN', '03443065905', 1, '2016-11-30', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
