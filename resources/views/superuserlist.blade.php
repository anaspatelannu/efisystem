@extends('layouts.app')
@section('content')
<!-- DataTables CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-responsive/css/datatable-responsive.css') }}" rel="stylesheet">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Super Users</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">
                    List Of Super Users
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <!--<th>Vendor Code</th>-->
                                    <th>Expiry Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($List as $vallist)
                                <tr>
                                    <td>{{ $vallist->UserName }}</td>
                                    <td>{{ $vallist->Email }}</td>
                                    <td>{{ $vallist->ContactNo }}</td>
                                    <!--<td>{{ $vallist->VendorCode }}</td>-->
                                    <td>{{ $vallist->ExpiryDate }}</td>
                                    <td><a href="{{ URL::to('/password/reset?'.$vallist->Email) }}" title="Reset Password">Reset Password</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>

<!-- Data Table Responsive JS -->
<script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/datatable-responsive.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/bootstrap-responsive.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable( {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal( {
                        header: function ( row ) {
                            var data = row.data();
                            return 'Details for '+data[0];
                        }
                    } ),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                        tableClass: 'table'
                    } )
                }
            }
        } );
    });
</script>
