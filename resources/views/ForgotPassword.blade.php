@extends('layouts.app')
@section('content')
<style>
    .backgroundcolorgreen{
        background-color: #009d57!important;
        background-image: url("{{ url('/public/images/slider5.jpg') }}");
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">
                    @if($ID == 1)
                        Forgot Password
                    @else 
                        Reset Password
                    @endif
                </div>
                <div class="panel-body">
                    @if($Msg != "")
                        <div class="alert alert-success" style="text-align:center">
                            {{ $Msg }}
                        </div>
                    @endif
                    <div class="alert alert-danger" style="text-align:center;display: none">
                        {{ $Msg }}
                    </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/ForgotPassword?ID=').$ID }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" required="true" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" id="create" class="btn btn-primary">
                                    <i class="fa fa-btn fa-lock"></i> Forgot Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).on('focusout', '#email', function () {
    if($('.messgae-container p').text() == "User Name Already Exists" || $('.messgae-container p').text() == "Vendor Code Already Exists"){
        return false;
    }
    var Email = $(this).val();
    $.ajax({
        type: "POST",
        url: "{{ URL::to('/checkemail') }}",
        data: {Email: Email, _token: $('input[name=_token]').val()},
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $('.alert').hide();
                $('.alert').text('');
                $('#create').removeAttr('disabled');
            }
            else {
                $('.alert').text('Email Not Exists')
                $('.alert').show();
                $('#create').attr('disabled','true');
                $("#email").focus();
                return false;
            }
        }
    });
});
</script>
