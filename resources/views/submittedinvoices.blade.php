@extends('layouts.app')
@section('content')
<!-- DataTables CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-responsive/css/datatable-responsive.css') }}" rel="stylesheet">
<link href="{{ URL::to('/public/css/jquery-ui.css') }}" rel="stylesheet">
<script src="{{ URL::to('/public/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('/public/js/jquery.number.js') }}"></script>

<style>
    th{
        text-align: center;
    }
    .SHNUMBER,.ID{
        display: none;
    }
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Submitted Invoice</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">
                    List Of Submitted Invoice
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            {{ csrf_field() }}
                            <thead>
                                <tr>
                                    @if($UserType == 'superuser')
                                    <th style="width: 25%;">Check To Cancel Invoice</th>
                                    @endif
                                    <th class="ID">Id</th>
                                    <th>Submitted By</th>
                                    <th class="SHNUMBER">Shipment Number</th>
                                    <th>Sap Invoice Number</th>
                                    <th>Invoice Reversal Number</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody align="center">
                                @foreach($List as $vallist)
                                @if($vallist->IsCancel == 1)
                                <tr style="background: #ffd7d7;">
                                    @if($UserType == 'superuser')
                                    <td class="checkboxes"><input type="checkbox" title="Check To Cancel Invoice" name="checkbox" id="{{ $vallist->ID }}" value="{{ $vallist->SAPINVOICE }}" disabled="true"></td>
                                    @endif
                                    <td class="ID">{{ $vallist->ID }}</td>
                                    <td>{{ $vallist->UserName }}</td>
                                    <td class="SHNUMBER">{{ $vallist->SHNUMBER }}</td>
                                    <td>{{ $vallist->SAPINVOICE }}</td>
                                    <td>{{ $vallist->CancelNumber }}</td>
                                    <td><a href="javascript:void(0)" class="detail" id="{{ $vallist->SAPINVOICE }}">Detail</a></td>
                                </tr>
                                @else
                                <tr>
                                    @if($UserType == 'superuser')
                                    <td class="checkboxes"><input type="checkbox" title="Check To Cancel Invoice" name="checkbox" id="{{ $vallist->ID }}" value="{{ $vallist->SAPINVOICE }}"></td>
                                    @endif
                                    <td class="ID">{{ $vallist->ID }}</td>
                                    <td>{{ $vallist->UserName }}</td>
                                    <td class="SHNUMBER">{{ $vallist->SHNUMBER }}</td>
                                    <td>{{ $vallist->SAPINVOICE }}</td>
                                    <td>{{ $vallist->CancelNumber }}</td>
                                    <td><a href="javascript:void(0)" class="detail" id="{{ $vallist->SAPINVOICE }}">Detail</a></td>
                                </tr>
                                @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-4">
                @if($UserType != 'contractor')
                <button type="submit" id="Reset" class="btn btn-primary " title="Reset">
                    <i class="fa fa-btn fa-undo"></i> Reset
                </button>
                <button type="submit" id="CancelSubmitInvoice" class="btn btn-primary " disabled="true" title="Cancel Invoice">
                    <i class="fa fa-btn fa-remove"></i> Cancel
                </button>
                @endif
            </div>
            <div class="col-lg-8"></div>
            <div class="messgae-container" style="display: none">
                <div class="status-message success-msg" style="width: 20%;float: right;">
                    <p class="message" style="padding-top: 5px;"> Cancel Successfully </p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" style="overflow-y: scroll">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>

                <div style="text-align: center" id="invoicedetail">

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary" id="SubmitCancelDetail" >Submit</button>
                    <button type="button" class="btn btn-primary dismiss" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModals" class="modal fade" style="overflow-y: scroll;">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>

                <div style="text-align: center" id="Submittedinvoicedetail">

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <button type="button" id="Print" class="btn btn-primary">Print</button>
                    <button type="button" class="btn btn-primary dismiss" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

    <script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
    
    <!-- Data Table Responsive JS -->
    <script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/datatable-responsive.js') }}"></script>
    <script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/bootstrap-responsive.js') }}"></script>
    
    <script>
     $(document).ready(function() {
        $('#dataTables-example').DataTable( {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal( {
                        header: function ( row ) {
                            var data = row.data();
                            return 'Details for '+data[2];
                        }
                    } ),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                        tableClass: 'table'
                    } )
                }
            },
            @if($UserType == 'superuser')
                "order": [[ 1, "desc" ]]
            @else
                "order": [[ 0, "desc" ]]
            @endif
        });
    });
     $(document).on('change', 'input[name=checkbox]', function () {
        var CheckBox;
        $('input[name=checkbox]:checked').map(function() {
            CheckBox = 1;
        });
        if(CheckBox == 1){
            $('#CancelSubmitInvoice').removeAttr('disabled');
        }
        else{
            $('#CancelSubmitInvoice').attr('disabled','true');
        }
    });
    
     $(document).on('click', '#Reset', function () {
         $('input:checkbox').removeAttr('checked');
         $('#CancelSubmitInvoice').attr('disabled','true');
    });
     
    $(document).on('click', '#CancelSubmitInvoice', function () {
            var checkboxValues = [];
            $('input[name=checkbox]:checked').map(function() {
                    checkboxValues.push($(this).val());
            });
            console.log(checkboxValues);
            
            var ids = [];
            $('input[name=checkbox]:checked').map(function() {
                    ids.push($(this).attr('id'));
            });
            console.log(ids);
            
            $('#invoicedetail').contents().remove();
            var HTML = "";
            HTML = HTML + "<h1>Provide Cancel Detail</h1>";
            HTML = HTML + "<div class='row'>";
            for(var i = 0 ;i<checkboxValues.length;i++){
                HTML = HTML + "<h4 style='float:left;margin-left:20px'>Sap Invoice Number : "+ checkboxValues[i] +"</h4>";
                HTML = HTML + "<input type='hidden' class='ShipmentNo' value='"+ checkboxValues[i] + "' />";
                HTML = HTML + "<input type='hidden' class='ShipmentID' value='"+ ids[i] + "' />";
                HTML = HTML + "<div class='col-lg-12 form-group'>";
                HTML = HTML + "<div class='form-group'>";
                HTML = HTML + "<div class='col-lg-2'>";
                HTML = HTML + "<p style='float:left'>Cancel Number</p>";
                HTML = HTML + "</div>";
                HTML = HTML + "<div class='col-lg-4'>";
                HTML = HTML + "<input type='text' class='form-control CancelNumber' value='' />";
                HTML = HTML + "</div>";
                HTML = HTML + "<div class='col-lg-2'>";
                HTML = HTML + "<p style='float:left'>Cancel Date</p>";
                HTML = HTML + "</div>";
                HTML = HTML + "<div class='col-lg-4'>";
                HTML = HTML + "<input type='text' class='datepicker form-control CancelDate' value='' />";
                HTML = HTML + "</div></div></div>";
            }
                HTML = HTML + "</div>";
                HTML = HTML + "<br>&nbsp;";
            $("#invoicedetail").append(HTML);
            
            $('.modal-footer').on('click', function (e) {
                $("#myModal a.btn").off("click");
                console.log("1");
            });
            $("#myModal").on("show", function () {    // wire up the OK button to dismiss the modal when shown
                $("#myModal a.btn").on("click", function (e) {
                    console.log("button pressed"); // just as an example...
                    $("#myModal").modal('hide'); // dismiss the dialog

                });
            });
            $("#myModal").on("hide", function () {    // remove the event listeners when the dialog is dismissed
                $("#myModal a.btn").off("click");
                console.log("1");
            });
            $("#myModal").on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
                $("#myModal").remove();
                console.log("1");
            });
            $("#myModal").modal({// wire up the actual modal functionality and show the dialog
                "backdrop": "static",
                "keyboard": true,
                "show": true                     // ensure the modal is shown immediately
            });
            
            $(".datepicker").datepicker({ dateFormat: 'dd.mm.yy' });
            
    });
    
    $(document).on('click', '#SubmitCancelDetail', function () {
        var ShipmentID = $('.ShipmentID');
        var ShipmentNo = $('.ShipmentNo');
        var CancelNumber = $('.CancelNumber');
        var CancelDate = $('.CancelDate');
        
        var ShipmentIDs = [];
        for(var i=0;i<ShipmentID.length;i++){
            var Number = ShipmentID[i];
            ShipmentIDs.push($(Number).val());
        } 
        console.log(ShipmentIDs);
        
        var ShipmentNos = [];
        for(var i=0;i<ShipmentNo.length;i++){
            var Number = ShipmentNo[i];
            ShipmentNos.push($(Number).val());
        } 
        console.log(ShipmentNos);
        
        var CancelNumbers = [];
        for(var i=0;i<CancelNumber.length;i++){
            var Number = CancelNumber[i];
            CancelNumbers.push($(Number).val());
        } 
        console.log(CancelNumbers);
        
        var CancelDates = [];
        for(var i=0;i<CancelDate.length;i++){
            var Number = CancelDate[i];
            CancelDates.push($(Number).val());
        } 
        console.log(CancelDates);
        $.ajax({
            type: "POST",
            url: "{{ URL::to('/cancelsubmittedinvoice') }}",
            data: {_token: $('input[name=_token]').val(),ShipmentID:ShipmentIDs, ShipmentNo: ShipmentNos,CancelNumber:CancelNumbers, CancelDate:CancelDates},
            success: function (data) {
                console.log("Invoice Cancel");
                $('.dismiss').click();
                $('input:checkbox:checked').parent().parent().remove();
                $('.messgae-container').show();
                $('.messgae-container').delay(5000).fadeOut();
            }
        });
    });
    
    $(document).on('click', '.detail', function (e){
            var SAPINVOICE = $(this).attr('id');
            
            $.ajax({
                type: "POST",
                url: "{{ URL::to('/SubmittedInvoiceDetail') }}",
                data: {_token: $('input[name=_token]').val(),SAPINVOICE:SAPINVOICE},
                success: function (data) {
                    var Data = jQuery.parseJSON(data);
                    var Total = 0;
                    console.log(Data);
                    if(Data != ""){
                        $('#Print').removeAttr('disabled');
                        var ShipNUMArray = [];
                        $('#Submittedinvoicedetail').contents().remove();

                        var HTML = "";
                        HTML = HTML + "<div id='print' style='overflow-x:auto;'>"
                        HTML = HTML + "<h1>Submitted Invoices</h1>";
                        HTML = HTML + "<h4><b>Name: "+Data[0].NAME1+"</b></h4>";
                        HTML = HTML + "<h4>Vendor Code: "+Data[0].LIFNR+"</h4>";
                        HTML = HTML + "<h4>Sap Invoice NO: "+Data[0].SAPINVOICE+"</h4>";
                        HTML = HTML + "<h4>Address: "+Data[0].F4+"</h4>";
                        HTML = HTML + "<h4>NTN NO: "+Data[0].F5+"</h4>";
                        HTML = HTML + "<h4>Contact NO: "+ {{ $UserDetail->ContactNo }} +"</h4>";
                                
                        HTML = HTML + "<table class='table table-striped table-bordered table-hover' style='border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;background-color: transparent;'>";
                        HTML = HTML + "<thead>";
                        HTML = HTML + "<tr>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Shipment Number</th>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Delivery Date</th>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Delivery Number</th>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Vehicle Text</th>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Quantity</th>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Unit</th>";
                        //HTML = HTML + "<th style='border: 1px solid #ddd'>Acknowledge</th>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Material</th>";
                        HTML = HTML + "<th style='border: 1px solid #ddd'>Amount</th>";
                        HTML = HTML + "</tr>";
                        HTML = HTML + "</thead>";
                        HTML = HTML + "<tbody align='center'>";

                        for(var i = 0;i<Data.length;i++){
                            if(jQuery.inArray(Data[i].SHNUMBER,ShipNUMArray) == -1){
                                
                                ShipNUMArray.push(Data[i].SHNUMBER);
                                var ShipNUM = Data[i].SHNUMBER;
                                var Count = 0;
                                for(var j=0;j<Data.length;j++){
                                    if(ShipNUM == Data[j].SHNUMBER){
                                        HTML = HTML + "<tr class="+Data[j].SHNUMBER+">";
                                        if(Count < 1){
                                            HTML = HTML + "<td class='ship' style='border: 1px solid #ddd'>"+Data[i].SHNUMBER+"</td>";
                                        }
                                        ++Count;
                                        HTML = HTML + "<td style='border: 1px solid #ddd'>"+Data[i].DELDATE+"</td>";
                                        HTML = HTML + "<td style='border: 1px solid #ddd'>"+Data[i].VBELN+"</td>";
                                        HTML = HTML + "<td style='border: 1px solid #ddd'>"+Data[i].F3+"</td>";
                                        HTML = HTML + "<td style='border: 1px solid #ddd'>"+Data[i].QTY+"</td>";
                                        HTML = HTML + "<td style='border: 1px solid #ddd'>"+Data[i].UNIT+"</td>";
                                        //HTML = HTML + "<td style='border: 1px solid #ddd'>"+Data[i].ACK+"</td>";
                                        HTML = HTML + "<td style='border: 1px solid #ddd'>"+Data[i].F2+"</td>";
                                        HTML = HTML + "<td class='Amount' style='border: 1px solid #ddd'>"+Data[i].AMOUNT+"</td>";
                                        HTML = HTML + "</tr>";
                                        Total += parseFloat(Data[i].AMOUNT);
                                    }
                                }
                                HTML = HTML + "<br>&nbsp;";
                            }
                        }
                        HTML = HTML + "</tbody>";
                        HTML = HTML + "</table>";
                        HTML = HTML + "<div style='float:right'><h4>Grand Total : <span id='GrandTotal'>" + Total + "</sapn></h4></div>";
                        HTML = HTML + "</div>";
                        
                        $("#Submittedinvoicedetail").append(HTML);
                        $('#GrandTotal').number( true, 2 );
                        $('.Amount').number( true, 2 );
                        for(var k = 0;k<ShipNUMArray.length;k++){
                            $('.'+ ShipNUMArray[k] +' td[class=ship]').attr('rowspan',$('.'+ ShipNUMArray[k]).length)
                        }
                    }
                    else{ 
                        $('#Submittedinvoicedetail').contents().remove();
                        var HTML = "";
                        HTML = HTML + "<h2>No Detail</h2>";
                        $('#Print').attr('disabled','true');
                        $("#Submittedinvoicedetail").append(HTML);
                    }
                }
            });
            
            
            
            $('.modal-footer').on('click', function (e) {
                $("#myModals a.btn").off("click");
                console.log("1");
            });
            $("#myModals").on("show", function () {    // wire up the OK button to dismiss the modal when shown
                $("#myModals a.btn").on("click", function (e) {
                    console.log("button pressed"); // just as an example...
                    $("#myModals").modal('hide'); // dismiss the dialog

                });
            });
            $("#myModals").on("hide", function () {    // remove the event listeners when the dialog is dismissed
                $("#myModals a.btn").off("click");
                console.log("1");
            });
            $("#myModals").on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
                $("#myModals").remove();
                console.log("1");
            });
            $("#myModals").modal({// wire up the actual modal functionality and show the dialog
                "backdrop": "static",
                "keyboard": true,
                "show": true                     // ensure the modal is shown immediately
            });
    });
    
    $(document).on('click', '#Print', function () {
        var divContents = $("#print").html();
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>Invoice Detail</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');        
        printWindow.document.close();
        printWindow.print();
    });
    
    </script>
