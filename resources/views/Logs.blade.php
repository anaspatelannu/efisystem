@extends('layouts.app')
@section('content')
<!-- DataTables CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-responsive/css/datatable-responsive.css') }}" rel="stylesheet">
<style>
    .ID{
        display: none;
    }
</style>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Logs</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">
                    Logs
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th class="ID">ID</th>
                                    <th>Description</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($List as $vallist)
                                <tr>
                                    <td class="ID">{{ $vallist->ID }}</td>
                                    <td>{{ $vallist->Description }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>

<!-- Data Table Responsive JS -->
<script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/datatable-responsive.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/bootstrap-responsive.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable( {
            "order": [[ 0, "desc" ]]
        } );
    });
</script>
