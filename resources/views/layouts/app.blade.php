<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>EFI System</title>
        <link rel="shortcut icon" type="image/x-icon" href="{{ URL::to('/public/icon.ico') }}" />
        <!-- Fonts -->
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">-->
        <link href="{{ URL::to('/public/css/font-awesome.css') }}" rel="stylesheet">
        <!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">-->

        <!-- Styles -->
        <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">-->
        <link href="{{ URL::to('/public/css/twitter-bootstrap.css') }}" rel="stylesheet">
        
        <link href="{{ URL::to('/public/js/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ URL::to('/public/js/bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">
        <link href="{{ URL::to('/public/js/dist/css/timeline.css') }}" rel="stylesheet">
        <link href="{{ URL::to('/public/js/dist/css/sb-admin-2.css') }}" rel="stylesheet">
        <link href="{{ URL::to('/public/js/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
        <link href="{{ URL::to('/public/js/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

        {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

        <style>
            body {
                font-family: 'Lato';
            }
            .dropdown-toggle,.Login{
                margin-right: 15px;
            }
            .fa-btn {
                margin-right: 6px;
            }
            .status-message{
                text-align: center;
                color: white;
                height: 30px;
                border-radius: 5px;
            }
            .error-msg{
                background-color: #c76464;
            }
            .success-msg{
                background-color: #1dd276;
            }
            .message{
                padding-top: 3px;
            }
            .messgae-container
            {
                padding-bottom: 50px;
            }
            .closed{
                -webkit-appearance: none;
                padding: 0;
                cursor: pointer;
                background: 0 0;
                border: 0;
                float: right;
                font-size: 21px;
                font-weight: bold;
                line-height: 1;
                color: #000;
                text-shadow: 0 1px 0 #fff;
                filter: alpha(opacity=20);
                opacity: .2;
            }
            .checkboxes{
                text-align: center;
            }
            .backgroundgreen{
                background: #009d57!important;
            }
            .backgroundcolorgreen{
                background-color: #009d57!important;
            }
            .backgroundblue{
                background: #00458d!important;
            }
            .backgroundyellow{
                background: rgb(255, 226, 26)!important;
            }
            .colorwhite{
                color: white!important;
            }
            .navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:focus, .navbar-default .navbar-nav>.open>a:hover{
                background-color: #00458d!important;
            }
            li a:hover{
                background: #00458d!important;
                color: white!important;
            }
            li a:focus{
                background: #00458d!important;
                color: white!important;
            }
            .pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover {
                z-index: 3;
                color: #fff;
                cursor: default;
                background-color: #00458d!important;
                border-color: #00458d!important;
            }
            .btn-primary{
                color: #fff!important;
                background-color: #00458d!important;
                border-color: #00458d!important;
            }
            .btn-primary:hover{
                color: #fff!important;
                background-color: #337ab7!important;
                border-color: #2e6da4!important;
            }
        </style>
    </head>
    <body id="app-layout" class="backgroundcolorgreen">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="containers">
                <div class="navbar-header backgroundgreen">
                    {{ csrf_field() }}
                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand colorwhite" href="{{ url('/') }}" title="EFI System">
                        <img src="{{ url('/public/images/logo.png') }}" alt="PSO" width="50px" style="float: left;margin-top: -7%" />EFI System
                    </a>
                </div>

                <div class="collapse navbar-collapse backgroundgreen" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <!--                <ul class="nav navbar-nav">
                                        <li><a href="{{ url('/home') }}">Home</a></li>
                                    </ul>-->

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if(session()->get('Name') == "")
                        <li><a href="{{ url('') }}" class="Login colorwhite">Login</a></li>
                        <!--<li><a href="{{ url('/register') }}">Register</a></li>-->
                        @else
                        <li class="dropdown">
                            <a href="javascript:void(0)" class="dropdown-toggle colorwhite" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ session()->get('UserName') }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Navigation -->
        @if(session()->get('Name'))
        <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse backgroundgreen">
                    <ul class="nav" id="side-menu">
                        @if(session()->get('Name') == "superadmin")
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/adminlist') }}" title="Show List Of Admins"><i class="fa fa-list fa-fw"></i> List Of Admins</a>
                        </li>
                        @endif
                        @if(session()->get('Name') == "superadmin" || session()->get('Name') == "admin")
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/superuserlist') }}" title="Show List Of Reviewer"><i class="fa fa-list fa-fw"></i> List Of Reviewer</a>
                        </li>
                        @endif
                        @if(session()->get('Name') == "superadmin" || session()->get('Name') == "admin" || session()->get('Name') == "superuser")
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/contractorlist') }}" title="Show List Of Contractor/Vendor"><i class="fa fa-list fa-fw"></i> List Of Contractors</a>
                        </li>
                        @endif
                        @if(session()->get('Name') == "superadmin")
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/Logs') }}" title="Track Activity"><i class="fa fa-list-alt fa-fw"></i> Log</a>
                        </li>
                        @endif
                        @if(session()->get('Name') == "superadmin" || session()->get('Name') == "admin")
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/dashboard') }}" title="Manage User"><i class="fa fa-user fa-fw"></i> User Management</a>
                        </li>
                        @endif
                        @if(session()->get('Name') == "contractor")
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/invoices') }}" title="Contractor's/Vendor's Invoice"><i class="fa fa-book fa-fw"></i> Invoices</a>
                        </li>
                        @endif
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/submittedinvoices') }}" title="Show Submitted Invoice"><i class="fa fa-book fa-fw"></i> Submitted Invoices</a>
                        </li>
                        <li>
                            <a class="colorwhite" href="{{ URL::to('/printinvoice') }}" title="Payment Summary"><i class="fa fa-print fa-fw"></i> Payment Summary</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
        @endif

        @yield('content')

        <!-- JavaScripts -->
        
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>-->
        <script src="{{ URL::to('/public/js/ajax-lib.js') }}"></script>
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
</body>
</html>


<script>
    setInterval(function(){ 
        $.ajax
                ({
                    type: 'POST',
                    url: "{{ URL::to('/UpdateLoginTime') }}",
                    data: {_token: $('input[name=_token]').val()},
                    success: function (row) {
                        console.log("Updated Login Time");
                        },
                    failure: function() {
                        console.log("Error in Updateing Login Time!");
                    }
                });
    }, 300000);
</script>
