@extends('layouts.app')
@section('content')

<!-- DataTables CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Payment Summary</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">
                    List Of Invoice
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            {{ csrf_field() }}
                            <thead>
                                <tr>
                                    <th>Vendors</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($directories as $folder)
                                <tr>
                                    <td>
                                    
                                        <a class="folders" href="javascript:void(0)" title="Click To Show Invoice" name="{{$folder}}" files="show">{{$folder}}</a>
                                        <div id="{{$folder}}"></div>
                                    
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>

<style>
    .folders{
        color: black;
        font-size: 18px;
    }
</style>
<script>
$(document).ready(function () {
    
    $('#dataTables-example').DataTable({
            responsive: true
    })

    $(document).on('click', '.folders', function (e) {
        var contractorId = $(this).attr('name');
        var ref = $(this);
        var isShow = $(ref).attr('files');
        if(isShow == 'show') {
            $.ajax({
                type: "POST",
                url: "{{ URL::to('/fetchpdfinvoices') }}",
                data: {
                    _token: $('input[name=_token]').val(),
                    folderName: contractorId
                },
                success: function (data) {
                    var Data = jQuery.parseJSON(data);
                    console.log(Data)
                    if (Data != "") {
                        var HTML = "";
                        HTML += "<ul id=" + contractorId + "_filesList class='filesList'>";
                        HTML += Data;
                        HTML += "</ul>";
                        $("#" + contractorId + "_filesList").remove();
                        $("#" + contractorId).append(HTML);
                    }
                    else {
                        var HTML = "";
                        HTML += "<ul><p id=" + contractorId + "_filesList>No invoice found</p></ul>";
                        $("#" + contractorId + "_filesList").remove();
                        $("#" + contractorId).append(HTML);
                    }
                }
            });
            $(ref).attr('files', 'hide');
        }
        else{
            var contractorId = $(this).attr('name');
            var ref = $(this);
            $("#" + contractorId + "_filesList").remove();
            $(ref).attr('files', 'show');
        }
    });
});
</script>