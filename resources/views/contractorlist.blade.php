@extends('layouts.app')
@section('content')
<!-- DataTables CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-responsive/css/datatable-responsive.css') }}" rel="stylesheet">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Contractors</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">
                    List Of Contractors
                </div>

                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            {{ csrf_field() }}
                            <thead>
                                <tr>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>Contact Number</th>
                                    <th>Vendor Code</th>
                                    <th>Expiry Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($List as $vallist)
                                <tr>
                                    <td>{{ $vallist->UserName }}</td>
                                    <td>{{ $vallist->Email }}</td>
                                    <td>{{ $vallist->ContactNo }}</td>
                                    <td>{{ $vallist->VendorCode }}</td>
                                    <td>{{ $vallist->ExpiryDate }}</td>
                                    <?php $temp = "Enable/Disable"; ?>
                                    <td><a href="{{ URL::to('/password/reset?'.$vallist->Email) }}" title="Reset Password">Reset Password</a><br>
                                        <a href="javascript:void(0)" class="Edit" ContractorID="{{ $vallist->ID }}"  >Edit Contractor</a><br>
                                        @if($vallist->$temp == 0)
                                            <a href="javascript:void(0)" class="Disable" id="{{ $vallist->ID }}"> Disable</a>
                                        @else
                                            <a href="javascript:void(0)" class="Enable" id="{{ $vallist->ID }}"> Enable</a>
                                        @endif
                                        <!--                                        <a href="{{ URL::to('/invoices?'.$vallist->UserName) }}">Show Invoice</a>-->
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" style="overflow-y: scroll">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>

                <div style="text-align: center" id="ContractorDetail">
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <input type="submit" id="Save" class="btn btn-primary" value="Save" />
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal1" class="modal fade" style="overflow-y: scroll">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>
                <br>
                <br>
                <div class="form-group" style="text-align: center">
                    Are You Sure You Want To Disable This Account ?
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Yes" />
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>

            </div>
        </div>
    </div>
</div>
<div id="myModal2" class="modal fade" style="overflow-y: scroll">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>
                <br>
                <br>
                <div class="form-group" style="text-align: center">
                    Are You Sure You Want To Enable This Account ?
                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Yes" />
                    <button type="button" class="btn btn-primary" data-dismiss="modal">No</button>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection

<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>

<!-- Data Table Responsive JS -->
<script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/datatable-responsive.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-responsive/js/bootstrap-responsive.js') }}"></script>

<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable( {
            responsive: {
                details: {
                    display: $.fn.dataTable.Responsive.display.modal( {
                        header: function ( row ) {
                            var data = row.data();
                            return 'Details for '+data[0];
                        }
                    } ),
                    renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                        tableClass: 'table'
                    } )
                }
            }
        } );
    });
    
    $(document).on('click', '.Edit', function (e){
        var ContractorID = $(this).attr('ContractorID');
        $.ajax
            ({
                type: 'POST',
                url: "{{ URL::to('/EditContractor') }}",
                data: {_token: $('input[name=_token]').val(),ContractorID : ContractorID},
                success: function (row) {
                    console.log("fetched");
                    var Data = jQuery.parseJSON(row);
                    console.log(Data)
                    
                    $('#ContractorDetail').contents().remove();
                    
                    var HTML = "";
                    HTML = HTML + "<div style='overflow-x:auto;'>";                    
                    HTML = HTML + "<h1>Edit</h1>";
                    
                    HTML = HTML + "<div class='messgae-container' style='display: none'>";
                    HTML = HTML + "<div class='status-message error-msg'>";
                    HTML = HTML + "<p class='message'></p>";
                    HTML = HTML + "</div></div>";
                    
                    HTML = HTML + "<div class='col-lg-12'>";
                    HTML = HTML + "<div class='row'>";
                    HTML = HTML + "<h5 style='float:left'><b>Address</b></h5>";
                    HTML = HTML + "<input class='form-control UpdateAddress' type='text' value='"+ Data.Address +"'>";
                    HTML = HTML + "</div>";
                    HTML = HTML + "</div>";
                    
                    HTML = HTML + "<div class='col-lg-12'>";
                    HTML = HTML + "<div class='row'>";
                    HTML = HTML + "<h5 style='float:left'><b>Contact Number</b></h5>";
                    HTML = HTML + "<input class='form-control UpdateContactNo' type='text' value='"+ Data.ContactNo +"'>";
                    HTML = HTML + "</div>";
                    HTML = HTML + "</div>";
                    
                    HTML = HTML + "<input type='hidden' id='ContractorID' value='"+ ContractorID +"' />";
                    
                    HTML = HTML + "</div>";
                    HTML = HTML + "<br>&nbsp;";
                    $("#ContractorDetail").append(HTML);

                    $('.modal-footer').on('click', function (e) {
                        $("#myModal a.btn").off("click");
                        console.log("1");
                    });
                    $("#myModal").on("show", function () {    // wire up the OK button to dismiss the modal when shown
                        $("#myModal a.btn").on("click", function (e) {
                            console.log("button pressed"); // just as an example...
                            $("#myModal").modal('hide'); // dismiss the dialog

                        });
                    });
                    $("#myModal").on("hide", function () {    // remove the event listeners when the dialog is dismissed
                        $("#myModal a.btn").off("click");
                        console.log("1");
                    });
                    $("#myModal").on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
                        $("#myModal").remove();
                        console.log("1");
                    });
                    $("#myModal").modal({// wire up the actual modal functionality and show the dialog
                        "backdrop": "static",
                        "keyboard": true,
                        "show": true                     // ensure the modal is shown immediately
                    });
                },
                failure: function() {
                    console.log("Error in Fetching!");
                }
            });
    });
    
    $(document).on('click', '#Save', function (e){
        var Address = $(".UpdateAddress").val();
        var ContactNo = $(".UpdateContactNo").val();
        var ContractorID = $("#ContractorID").val();
        $.ajax
        ({
            type: 'POST',
            url: "{{ URL::to('/UpdateContractor') }}",
            data: {_token: $('input[name=_token]').val(),Address : Address,ContactNo : ContactNo,ContractorID:ContractorID},
            success: function (row) {
                console.log("Updated Successfully");
                $("#myModal").modal('hide');
                },
            failure: function() {
                console.log("Error in Update!");
            }
        });
    });
    
    $(document).on('click', '.Disable', function (e){
        var ID = $(this).attr('id')
        var Curr = $(this);
        
        $('.modal-footer').on('click', function (e) {
            $("#myModal1 a.btn").off("click");
            console.log("1");
            $.ajax
                ({
                    type: 'POST',
                    url: "{{ URL::to('/DisableUser') }}",
                    data: {_token: $('input[name=_token]').val(),ID : ID},
                    success: function (row) {
                        console.log("Disabled Successfully");
                        window.location.reload();
                            //$(Curr).text('Enable');
                            //$(Curr).attr('class','Enable');
                            //$("#myModal1").modal('hide');
                        },
                    failure: function() {
                        console.log("Error in Disabling Account!");
                    }
                });
        });
        $("#myModal1").on("show", function () {    // wire up the OK button to dismiss the modal when shown
            $("#myModal1 a.btn").on("click", function (e) {
                console.log("button pressed"); // just as an example...
                $("#myModal1").modal('hide'); // dismiss the dialog
                

            });
        });
        $("#myModal1").on("hide", function () {    // remove the event listeners when the dialog is dismissed
            $("#myModal1 a.btn").off("click");
            console.log("1");
        });
        $("#myModal1").on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
            $("#myModal1").remove();
            console.log("1");
        });
        $("#myModal1").modal({// wire up the actual modal functionality and show the dialog
            "backdrop": "static",
            "keyboard": true,
            "show": true                     // ensure the modal is shown immediately
        });
    });
    
    $(document).on('click', '.Enable', function (e){
        var ID = $(this).attr('id')
        var Curr = $(this);
        
        $('.modal-footer').on('click', function (e) {
            $("#myModal2 a.btn").off("click");
            console.log("1");
            $.ajax
                ({
                    type: 'POST',
                    url: "{{ URL::to('/EnableUser') }}",
                    data: {_token: $('input[name=_token]').val(),ID : ID},
                    success: function (row) {
                        console.log("Enabled Successfully");
                        window.location.reload();
                        //$(Curr).text('Disable');
                        //$(Curr).attr('class','Disable');
                        //$("#myModal2").modal('hide');
                        },
                    failure: function() {
                        console.log("Error in Enableing Account!");
                    }
                });
        });
        $("#myModal2").on("show", function () {    // wire up the OK button to dismiss the modal when shown
            $("#myModal2 a.btn").on("click", function (e) {
                console.log("button pressed"); // just as an example...
                $("#myModal2").modal('hide'); // dismiss the dialog
                

            });
        });
        $("#myModal2").on("hide", function () {    // remove the event listeners when the dialog is dismissed
            $("#myModal2 a.btn").off("click");
            console.log("1");
        });
        $("#myModal2").on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
            $("#myModal2").remove();
            console.log("1");
        });
        $("#myModal2").modal({// wire up the actual modal functionality and show the dialog
            "backdrop": "static",
            "keyboard": true,
            "show": true                     // ensure the modal is shown immediately
        });
    });
</script>
