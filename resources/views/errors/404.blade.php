@extends('layouts.app')
@section('content') 
<link rel="stylesheet" type="text/css" href="{{URL::to('/public/css/application.css')}}">
<style>
    .backgroundcolorgreen{
        background-color: white!important;
    }
    .error-info{
        margin-top: 10%!important;
    }
</style>

<div class=" dashboard error-404-page ">
    <article class=error-info >
         <div class=error-image></div>
         <p class=error-title>Awww, No!</p>
         <p class=error-description>Looks like the page you’re looking for is missing or doesn’t exist! Please check the URL and try again or:</p>
         <a class="small-button phocus-font-13" href="./">HEAD HOME</a>
    </article>
</div>

@endsection