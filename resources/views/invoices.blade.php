@extends('layouts.app')
@section('content')
<!-- DataTables CSS -->
<link href="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

<!-- DataTables Responsive CSS -->
<!--<link href="{{ URL::to('/public/js/bower_components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">-->
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">-->
<link href="{{ URL::to('/public/css/jquery-ui.css') }}" rel="stylesheet">
<!--<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>-->
<script src="{{ URL::to('/public/js/jquery-ui.js') }}"></script>
<script src="{{ URL::to('/public/js/jquery.number.js') }}"></script>
<script>
        $(function() {
            $(".datepicker").datepicker({ dateFormat: 'dd.mm.yy' });
        });
        $("#contractorId").selectmenu();
</script>
<style>
    fieldset {
        border: 0;
    }
    label {
        display: block;
        margin: 30px 0 0 0;
    }
    .overflow {
        height: 200px;
    }
    th{
        text-align: center;
    }
</style>


<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Invoices</h1>
            <div class="row">
                <div class="col-lg-0 form-group" style="display:none">
                    @if($UserType != 'contractor')
                    Contractor ID:
                    <select name="contractorId" id="contractorId" class="form-control">
                        @if($UserType == 'contractor')
                        <option contractorid="{{ $UserID }}">{{ $UserName }}</option>
                        @else
                        @foreach($contractorsList as $contractor)
                        <option contractorid="{{ $contractor->VendorCode }}">{{ $contractor->UserName }}</option>
                        @endforeach
                        @endif
                    </select>
                    @else
                    <select name="contractorId" id="contractorId" class="form-control" style="display: none">
                        @if($UserType == 'contractor')
                        <option contractorid="{{ $VendorCode }}">{{ $UserName }}</option>
                        @else
                        @foreach($contractorsList as $contractor)
                        <option contractorid="{{ $contractor->VendorCode }}">{{ $contractor->UserName }}</option>
                        @endforeach
                        @endif
                    </select>
                    @endif
                </div>
                <div class="col-lg-3 form-group">
                    From Date: <input type="text" name="fromDate" id="fromDate" class="FromDate datepicker form-control" />
                </div>
                <div class="col-lg-3 form-group">
                    To Date: <input type="text" name="toDate" id="toDate" class="ToDate datepicker form-control" />
                </div>
                <div class="col-lg-3 form-group" style="padding-top: 21px">
                    <button type="submit" id="fetchinvoice" title="Fetch Invoice" class="btn btn-primary">
                        <i class="fa fa-btn fa-arrow-down"></i> Fetch Invoice
                    </button>
                    <img id="load" src="{{ URL::to('/public/images/load.gif') }}" style="display: none" />
                </div>
                <div class="col-lg-3 form-group">
                    <div class="ErrorDate" style="display: none;padding-top: 21px">
                        <div class="status-message error-msg" style="float: right;height: 50px;">
                            <p class="message" style="padding: 5px 10px 10px 10px;"> From Date Should be greater than or equal To Date </p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-3">
                    <div class="form-group">
                        Shipment Number: <input type="text" name="shipmentNo" id="shipmentNo" class="form-control" />
                    </div>
                </div>
                <div class="col-lg-3" style="padding-top: 21px;">
                    <button type="submit" id="addinvoice" title="Add Invoice" class="btn btn-primary" >
                        <i class="fa fa-btn fa-plus-square"></i> Add
                    </button>
                    <button type="submit" id="removeinvoice" title="Remove Invoice" class="btn btn-primary" disabled="true">
                        <i class="fa fa-btn fa-trash"></i> Remove
                    </button>
                    <img id="loaders" src="{{ URL::to('/public/images/load.gif') }}" style="display: none" />
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">
                    Invoices
                </div>
                {{ csrf_field() }}
                <!--<span id="invoice"></span>-->
                <div class="panel-body">
                    <div class="dataTable_wrapper" id="InvoiceTable" style="overflow-x:auto;">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            
                            <thead align="center">
                                <tr>
                                    <!--<th>Shipment Number</th>-->
                                    <!--<th>Name</th>-->
                                    <!--<th>Shipment Number</th>-->
                                    <!--<th>LIFNR</th>-->
                                    <th>Check To Submit</th>
                                    <th>Shipment Number</th>
                                    <th>Vehicle Text</th>
                                    <th>Quantity</th>
                                    <th>Delivery Number</th>
                                    <th>Delivery Date</th>
                                    <th>Material</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody align="center" id="invoice">
                                @if($List != null)
                                @foreach($List as $vallist)
                                <tr>
                                    <td class="checkboxes"><input type="checkbox" name="checkbox" value="{{ $vallist->SHNUMBER }}"></td>
                                    <td>{{ $vallist->NAME1 }}</td>
                                    <td>{{ $vallist->SHNUMBER }}</td>
                                    <td>{{ $vallist->VEHICLE }}</td>
                                    <td>{{ $vallist->AMOUNT }}</td>
                                    <td>{{ $vallist->QTY }}</td>     
                                    <!--<td><a href="javascript:void(0)" class="detail" id="{{ $vallist->ID }}">Detail</a></td> {{ URL::to('/password/reset?'.$vallist->NAME1) }} -->
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td valign="top" colspan="9" class="dataTables_empty">No data available in table</td>
                                </tr>
                                @endif
                            </tbody>
                            <tbody align="center">
                                <tr>
                                    <tr style='border-color:black;border-width:2px;border-style:solid'>
                                        <td colspan='3'>Seleted Shipment:</td>
                                        <td colspan='1' id='SelectedInvoice' >0</td>
                                        <td colspan='3'>Gross Total:</td>
                                        <td colspan='1'id='TotalAmount' >0</td>
                                        <td ></td>
                                    </tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="col-lg-4">
                <button type="submit" id="Reset" class="btn btn-primary " title="Reset">
                    <i class="fa fa-btn fa-undo"></i> Reset
                </button>
                <button type="submit" id="SubmitInvoice" class="btn btn-primary " disabled="true" title="Submit Invoice">
                    <i class="fa fa-btn fa-arrow-up"></i> Submit
                </button>
                <img id="loader" src="{{ URL::to('/public/images/load.gif') }}" style="display: none" />
            </div>
<!--            <div class="col-lg-8">
                <h4 style="float:right">
                    Selected Shipment : <span id="SelectedInvoice">0</span>
                    &nbsp;&nbsp;&nbsp;
                    Gross Total : <span id="TotalAmount">0</span>
                </h4>
            </div>-->
        </div>
    </div>
    <div class="messgae-container messgae-containerSuccess" style="display: none">
        <div class="status-message success-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Submitted Successfully </p>
        </div>
    </div>
    <div class="messgae-container messgae-containerfetchedinvoice" style="display: none">
        <div class="status-message success-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Invoice Fetched </p>
        </div>
    </div>
    <div class="messgae-container messgae-containerremove" style="display: none">
        <div class="status-message success-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Removed </p>
        </div>
    </div>
    <div class="messgae-containers messgae-container" style="display: none">
        <div class="status-message error-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Shipment not found, please re-check shipment or date </p>
        </div>
    </div>
    <div class="servererror messgae-container" style="display: none">
        <div class="status-message error-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Shipment not found, please re-check shipment or date </p>
        </div>
    </div>
    <div class="messgae-container messageShipmentNotFound" style="display: none">
        <div class="status-message error-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> No Invoice </p>
        </div>
    </div>
    <div class="messgae-container hosterror" style="display: none">
        <div class="status-message error-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Could not connect to host </p>
        </div>
    </div>
    <div class="messgae-containerAlreadyFetch messgae-container" style="display: none">
        <div class="status-message error-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Already Fetched </p>
        </div>
    </div>
    <div class="messgae-containerservererror messgae-container" style="display: none">
        <div class="status-message error-msg" style="float: right;">
            <p class="message" style="padding: 5px 10px 10px 10px;"> Server/Host Error </p>
        </div>
    </div>
</div>
<div id="myModal" class="modal fade" style="overflow-y: scroll">
    <div class="modal-dialog" style="width: 90%;">
        <div class="modal-content">
            <!-- dialog body -->
            <div class="modal-body">
                <button type="button" class="closed" data-dismiss="modal">&times;</button>

                <div style="text-align: center" id="invoicedetail">

                </div>
                <!-- dialog buttons -->
                <div class="modal-footer">
                    <!--<button type="button" id="Print" class="btn btn-primary">Print</button>-->
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
</div>
<input type="hidden" id="ShipmentNOCheck" value="0">
<input type="hidden" id="ShipmentRowCheck" value="0">
@endsection


<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>

<script>
        //Intialize/Create function
        Array.prototype.removeValue = function(name, value){
            var array = $.map(this, function(v,i){
               return v[name] === value ? null : v;
            });
            this.length = 0; //clear original array
            this.push.apply(this, array); //push all elements except the one we want to delete
         }
        
        var prev_contractor = "";
        
        @if($fDate)
            var prevFromDate = {!! $fDate !!}; 
        @else
            var prevFromDate = "";
        @endif
        
        @if($tDate)
            var prevToDate = {!! $tDate !!};
        @else
            var prevToDate = "";
        @endif
        
        var prevShipmentNo = "";
        
        @if($FetchedInvoices)
            var storedData = {!! $FetchedInvoices !!};
        @else
            var storedData = "";
        @endif
        
        @if($FetchedInvoices)    
            var FileName = "{{ $filename }}";
        @else
            var FileName = "";
        @endif
        console.log(storedData);
        var ShipArray = []; 
        var Initial = 0;


        $(document).ready(function () {
            @if($UserType != 'contractor')
                $('#contractorId').val('')
            @endif
            $('#TotalAmount').number( true, 2 );
            $('.Amount').number( true, 2 );
            //Set Rows
            $('#fetchinvoice').attr('disabled','true');
            $('#loaders').show();
            
            $("#fromDate").val(prevFromDate);
            $("#toDate").val(prevToDate);
            
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();
            
            var contractorId = $('#contractorId option:selected').attr('contractorid')
            var shipmentNo = $("#shipmentNo").val();
            var array = new Array();
            if (storedData != "") {
                for (var i = 0; i < storedData.length; i++) {
                    array.push(storedData[i].SHNUMBER);
                }
                console.log(array);
                if(jQuery.inArray(shipmentNo,array) == -1){
                    prevShipmentNo = false;
                }
                else{
                    prevShipmentNo = true;
                }
            }
            console.log(prevShipmentNo);
            var flag = false;
            $('.messgae-container').hide();

            if(jQuery.inArray(shipmentNo,ShipArray) != -1){
                $('.messgae-containerAlreadyFetch').show();
                $('#fetchinvoice').removeAttr('disabled');
                $('#loaders').hide();
                return false;
            }
           
            if (storedData != "") { 
                if($('.panel-body tbody[id="invoice"] tr').last().text().trim() == "No data available in table" || $('.panel-body tbody[id="invoice"] tr').last().text().trim() == "No Invoice"){ 
                    $('#invoice tr').remove()
                }
                $('.page-header').html('Invoices Of '+storedData[0].NAME1+'<br />Vendor Code '+storedData[0].LIFNR)
                var HTML = "";
                var count = 0;
                for (var i = 0; i < storedData.length; i++) { 
                    if (storedData[i].Status == 1) {
                        Initial = 1;
                        var CheckID = storedData[i].F3;
                        HTML = HTML + "<tr class='"+ CheckID +"' name="+storedData[i].SHNUMBER+">";
                        HTML = HTML + "<input type='hidden' class='"+ storedData[i].SHNUMBER + "' name='CheckBox' value='"+ storedData[i].SHNUMBER + "' id='" + storedData[i].LIFNR + "' LIFNR='"+ storedData[i].LIFNR +"' NAME1='"+ storedData[i].NAME1 +"' SHNUMBER='"+ storedData[i].SHNUMBER  +"' DELDATE='"+ storedData[i].DELDATE +"' VBELN='"+ storedData[i].VBELN +"' VEHICLE='"+ storedData[i].F3 +"' AMOUNT='"+ storedData[i].AMOUNT +"' QTY='"+storedData[i].QTY +"' UNIT='"+ storedData[i].UNIT +"' ACK='"+ storedData[i].ACK +"' F1='"+ storedData[i].F1 +"' F2='"+ storedData[i].F2 +"' F3='"+ storedData[i].F3 +"' F4='"+ storedData[i].F4 +"' F5='"+ storedData[i].F5 +"' RBLGP='"+ storedData[i].RBLGP +"' />";
                        
                        var shipmentNo = storedData[i].SHNUMBER;
                        $('#ShipmentRowCheck').val(++count);
                        
//                        if($('#ShipmentNOCheck').val() == 0){
//                            HTML = HTML + "<td rowspan='1' class='" + storedData[i].SHNUMBER + "' style='text-align:center'><input type='checkbox' title='Check To Submit Invoice' class='"+ shipmentNo +"' name='checkbox' value='"+ shipmentNo + "' ></td>";
//                            HTML = HTML + "<td rowspan='1' class='" + storedData[i].SHNUMBER + "'>" + storedData[i].SHNUMBER + "</td>";
//                            $('#ShipmentNOCheck').val('1');
//                        }
                        HTML = HTML + "<td rowspan='1' class='" + storedData[i].SHNUMBER + "' style='text-align:center'><input type='checkbox' title='Check To Submit Invoice' class='"+ shipmentNo +"' name='checkbox' value='"+ shipmentNo + "' ></td>";
                        HTML = HTML + "<td rowspan='1' class='" + storedData[i].SHNUMBER + "'>" + storedData[i].SHNUMBER + "</td>";
                        HTML = HTML + "<td>" + storedData[i].F3 + "</td>";
                        HTML = HTML + "<td>" + storedData[i].QTY + "</td>";
                        HTML = HTML + "<td>" + storedData[i].VBELN + "</td>";
                        HTML = HTML + "<td>" + storedData[i].DELDATE + "</td>";
                        HTML = HTML + "<td>" + storedData[i].F2 + "</td>";
                        HTML = HTML + "<td class='Amount'>" + storedData[i].AMOUNT + "</td>";
                        HTML = HTML + "<td><a href='javascript:void(0)' class='detail' id='" + storedData[i].LIFNR + "' LIFNR='"+ storedData[i].LIFNR +"' NAME1='"+ storedData[i].NAME1 +"' SHNUMBER='"+ storedData[i].SHNUMBER  +"' DELDATE='"+ storedData[i].DELDATE +"' VBELN='"+ storedData[i].VBELN +"' VEHICLE='"+ storedData[i].F3 +"' AMOUNT='"+ storedData[i].AMOUNT +"' QTY='"+storedData[i].QTY +"' UNIT='"+ storedData[i].UNIT +"' ACK='"+ storedData[i].ACK +"' F1='"+ storedData[i].F1 +"' F2='"+ storedData[i].F2 +"' F3='"+ storedData[i].F3 +"' F4='"+ storedData[i].F4 +"' F5='"+ storedData[i].F5 +"' RBLGP='"+ storedData[i].RBLGP +"' >Detail</a></td>";
                        HTML = HTML + "</tr>";
                        
//                        HTML = HTML + "<tr style='border-color:black;border-width:2px;border-style:solid'>";
//                        HTML = HTML + "<td colspan='3'>Seleted Shipment:</td>";
//                        HTML = HTML + "<td colspan='1' id='SelectedInvoice' >0</td>";
//                        HTML = HTML + "<td colspan='3'>Gross Total:</td>";
//                        HTML = HTML + "<td colspan='1'id='TotalAmount' >0</td>";
//                        HTML = HTML + "<td ></td>";
//                        HTML = HTML + "</tr>";
                        
                        
                        flag = true;
                        storedData[i].Status = "1";
                        
//                        FileName = {{ $UserID }}+"_"+fromDate+"_"+toDate+".json"
                        
//                        $.ajax
//                        ({
//                            type: 'POST',
//                            url: "{{ URL::to('/UpdateJSON') }}",
//                            data: {_token: $('input[name=_token]').val(),StoredData : storedData , FileName : FileName},
//                            success: function () {
//                                console.log("JSON File Updated")
//                            },
//                            failure: function() {
//                                console.log("Error in submission!")
//                            }
//                        });
                    }
                }
                if(flag){
                    ShipArray.push(shipmentNo);
                }
                $('#ShipmentNOCheck').val('0');
                $("#invoice").append(HTML);
                console.log($('#ShipmentRowCheck').val());
                console.log(CheckID);
                console.log(shipmentNo);
                //$('.'+CheckID+' td[class="'+shipmentNo+'"]').attr('rowspan',$('#ShipmentRowCheck').val());
                $('#ShipmentRowCheck').val('0');
            }
            else{ 
                //$('.messgae-containerSuccess').hide();
                //$('.messgae-containers').show();
                if(Initial != 0){
                    $(".panel-body").last().remove()
                }
                else{
                    $('#invoice tr').remove()
                    $('.page-header').html('Invoices')
                    var HTML = "";
                    HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No Invoice</td></tr>";
                    $("#invoice").append(HTML);
                }
                }
            if (!flag){ 
                //$('.messgae-containerSuccess').hide();
                //$('.messgae-containers').show();
                if(Initial != 0){ 
                    //$(".panel-body").last().remove()
                }
                else{ 
                    $('#invoice tr').remove()
                    $('.page-header').html('Invoices')
                    var HTML = "";
                    HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No Invoice</td></tr>";
                    $("#invoice").append(HTML);
                }
            }
            $('#fetchinvoice').removeAttr('disabled');
            $('#loaders').hide();
            prev_contractor = $('#contractorId option:selected').attr('contractorid')
            prevFromDate = $("#fromDate").val();
            prevToDate = $("#toDate").val();
            prevShipmentNo = $("#shipmentNo").val();
            $('.Amount').number( true, 2 );
            //End Setting Rows
        });
        
        $(document).on('click', '#fetchinvoice', function () { 
        
            $('#fetchinvoice').attr('disabled','true');
            $('#load').show();
            
            var contractorId = $('#contractorId option:selected').attr('contractorid')
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();
            var shipmentNo = $("#shipmentNo").val();
            var Datacheck = 0;
            $('.messgae-container').hide();
            
            if (prevToDate != toDate || prevFromDate != fromDate){
                //removing Rows
                prev_contractor = "";
                //prevFromDate = "";
                //prevToDate = "";
                prevShipmentNo = "";
                storedData = "";
                //$('#fromDate').val('');
                //$('#toDate').val('');
                $('#shipmentNo').val('');
                //$('#contractorId').val('')
                ShipArray = [];
                Initial = 0;
                $('#TotalAmount').text('0');
                $('#TotalAmount').number( true, 2 );
                $('#SelectedInvoice').text('0');
                $('.page-header').html('Invoices')
                $('#invoice tr').remove()
                var HTML = "";
                HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No data available in table</td></tr>";
                $("#invoice").append(HTML);
                
                $.ajax({
                    type: "POST",
                    url: "{{ URL::to('/fetchinvoice') }}",
                    data: {Fetch: '1', _token: $('input[name=_token]').val(), fromDate: fromDate, toDate:toDate, contractorId:contractorId, shipmentNo:shipmentNo},
                    success: function (data) {
                        var Data = jQuery.parseJSON(data);
                        storedData = Data;
                        console.log(Data)
                        if (Data != "") {
                            //Initial = 1;
                            if(Data == "1"){
                                $('.servererror p').text("Could not connect to host");
                                $('.servererror').show();
                            }
                            else if(Data == "2"){
                                $('.servererror p').text("Error Fetching http headers");
                                $('.servererror').show();
                            }
                            else if(Data == "3"){
                                $('.servererror p').text("Server Error");
                                $('.servererror').show();
                            }
                            else if(Data == "4"){
                                $('.servererror p').text("Service Unavailable");
                                $('.servererror').show();
                            }
                            else{
                                $('.messgae-containerfetchedinvoice').show();
                            }
                        }
                        else
                        { 
                            $('.messgae-containerSuccess').hide();
                            $('.messageShipmentNotFound').show();
                            Datacheck = 1;
                        }

                        $('#fetchinvoice').removeAttr('disabled');
                       $('#load').hide();
                        prev_contractor = $('#contractorId option:selected').attr('contractorid');
                        if(Datacheck != 1){ 
                            prevFromDate = $("#fromDate").val();
                            prevToDate = $("#toDate").val();
                            $('#addinvoice').removeAttr('disabled');
                        }
                    }
                });
            }else{ 
                $('.messgae-containerSuccess').hide();
                $('.messgae-containerfetchedinvoice').show();
                //$('.messgae-containers').show();
                $('#fetchinvoice').removeAttr('disabled');
                $('#load').hide();
            }
            
        });
        $(document).on('click', '.detail', function (e){
            var id = $(this).attr('id');
            var NAME1 = $(this).attr('NAME1');
            var LIFNR = $(this).attr('LIFNR');
            var SHNUMBER = $(this).attr('SHNUMBER');
            var DELDATE = $(this).attr('DELDATE');
            var VBELN = $(this).attr('VBELN');
            var VEHICLE = $(this).attr('VEHICLE');
            var AMOUNT = $(this).attr('AMOUNT');
            var QTY = $(this).attr('QTY');
            var UNIT = $(this).attr('UNIT');
            var ACK = $(this).attr('ACK');
            var F1 = $(this).attr('F1');
            var F2 = $(this).attr('F2');
            var F3 = $(this).attr('F3');
            var F4 = $(this).attr('F4');
            var F5 = $(this).attr('F5');
            var RBLGP = $(this).attr('RBLGP');
            
            $('#invoicedetail').contents().remove();
            
            var HTML = "";
            HTML = HTML + "<div id='print' style='overflow-x:auto;'>"
            HTML = HTML + "<h1>Detail</h1>";
            HTML = HTML + "<h4>Name: "+ NAME1 +"</h4>";
            HTML = HTML + "<h4>Vendor Code: "+ LIFNR +"</h4>";
            HTML = HTML + "<h4>Shipment Number: "+ SHNUMBER +"</h4>";
            HTML = HTML + "<table class='table table-striped table-bordered table-hover' id='printtable' style='border: 1px solid #ddd;width: 100%;max-width: 100%;margin-bottom: 20px;border-spacing: 0;border-collapse: collapse;background-color: transparent;'>";
            HTML = HTML + "<thead>";
            HTML = HTML + "<tr>";
            HTML = HTML + "<th style='border: 1px solid #ddd'>Delivery Date</th>";
            HTML = HTML + "<th style='border: 1px solid #ddd'>Delivery Number</th>";
            HTML = HTML + "<th style='border: 1px solid #ddd'>Vehicle Text</th>";
            HTML = HTML + "<th style='border: 1px solid #ddd'>Qunatity</th>";
            HTML = HTML + "<th style='border: 1px solid #ddd'>Unit</th>";
            HTML = HTML + "<th style='border: 1px solid #ddd'>Material</th>";
            HTML = HTML + "<th style='border: 1px solid #ddd'>Amount</th>";
            HTML = HTML + "</tr>";
            HTML = HTML + "</thead>";
            HTML = HTML + "<tbody align='center'>";
            HTML = HTML + "<tr>";
            HTML = HTML + "<td style='border: 1px solid #ddd'>"+DELDATE+"</td>";
            HTML = HTML + "<td style='border: 1px solid #ddd'>"+VBELN+"</td>";
            HTML = HTML + "<td style='border: 1px solid #ddd'>"+F3+"</td>";
            HTML = HTML + "<td style='border: 1px solid #ddd'>"+QTY+"</td>";
            HTML = HTML + "<td style='border: 1px solid #ddd'>"+UNIT+"</td>";
            HTML = HTML + "<td style='border: 1px solid #ddd'>"+F2+"</td>";
            HTML = HTML + "<td class='Amount' style='border: 1px solid #ddd'>"+AMOUNT+"</td>";
            HTML = HTML + "</tr>";
            HTML = HTML + "</tbody>";
            HTML = HTML + "</table>";
            HTML = HTML + "</div>";
            HTML = HTML + "<br>&nbsp;";
            $("#invoicedetail").append(HTML);
            $('.Amount').number( true, 2 );
            
            $('.modal-footer').on('click', function (e) {
                $("#myModal a.btn").off("click");
                console.log("1");
            });
            $("#myModal").on("show", function () {    // wire up the OK button to dismiss the modal when shown
                $("#myModal a.btn").on("click", function (e) {
                    console.log("button pressed"); // just as an example...
                    $("#myModal").modal('hide'); // dismiss the dialog

                });
            });
            $("#myModal").on("hide", function () {    // remove the event listeners when the dialog is dismissed
                $("#myModal a.btn").off("click");
                console.log("1");
            });
            $("#myModal").on("hidden", function () {  // remove the actual elements from the DOM when fully hidden
                $("#myModal").remove();
                console.log("1");
            });
            $("#myModal").modal({// wire up the actual modal functionality and show the dialog
                "backdrop": "static",
                "keyboard": true,
                "show": true                     // ensure the modal is shown immediately
            });
    });

    $(document).on('click', '#Reset', function () {
        var fromDate = $("#fromDate").val();
        var toDate = $("#toDate").val();
        for(var i=0;i<storedData.length;i++){
            storedData[i].Status = "0";
        }

        FileName = {{ $UserID }}+"_"+fromDate+"_"+toDate+".json"
        
        $.ajax
        ({
            type: 'POST',
            url: "{{ URL::to('/UpdateJSON') }}",
            data: {_token: $('input[name=_token]').val(),StoredData : storedData , FileName : FileName},
            success: function () {
                console.log("JSON File Updated")
            },
            failure: function() {
                console.log("Error in submission!")
            }
        });
        prev_contractor = "";
        prevFromDate = "";
        prevToDate = "";
        prevShipmentNo = "";
        storedData = "";
        $('#fromDate').val('');
        $('#toDate').val('');
        $('#shipmentNo').val('');
        //$('#contractorId').val('')
        ShipArray = [];
        Initial = 0;
        $('#TotalAmount').text('0');
        $('#TotalAmount').number( true, 2 );
        $('#SelectedInvoice').text('0');
        $('.page-header').html('Invoices')
        $('#invoice tr').remove()
        var HTML = "";
        HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No data available in table</td></tr>";
        $("#invoice").append(HTML);

    });
    
    $(document).on('change', 'input[name=checkbox]', function () {
        var CheckBox;
        var Total = 0;
        $('input[name=checkbox]:checked').map(function() {
            CheckBox = 1;            
            var Class = $(this).attr('class');
            for (var i =0 ; i<$('.'+Class+':input[name=CheckBox]').length;i++){
                Total += parseFloat($($('.'+Class+':input[name=CheckBox]')[i]).attr('amount'));
            }
        });
        $('#TotalAmount').text(Total);
        $('#TotalAmount').number( true, 2 );
        $('#SelectedInvoice').text($('input[name=checkbox]:checked').length);
        if(CheckBox == 1){
            $('#SubmitInvoice').removeAttr('disabled');
            $('#removeinvoice').removeAttr('disabled');
        }
        else{
            $('#SubmitInvoice').attr('disabled','true');
            $('#removeinvoice').attr('disabled','true');
        }
        CheckBox = 0;
    });
    
    $(document).on('click', '#SubmitInvoice', function () {
        $('#removeinvoice').attr('disabled','true');
        $('#SubmitInvoice').attr('disabled','true');
        $('#loader').show();
        $('.messgae-container').hide();
        var checkboxValues = [];
        var temp = Array()
        var i=1;
        $('input[name=checkbox]:checked').map(function(val , i) {
                var Class = $(this).attr('class');
                var Input = $("."+Class+":input[name=CheckBox]");
                for(var i = 0; i <$("."+Class+":input[name=CheckBox]").length; i++){
                    checkboxValues =Array();
                    checkboxValues.push($(Input[i]).val());
                    checkboxValues.push($(Input[i]).attr('LIFNR'));
                    checkboxValues.push($(Input[i]).attr('NAME1'));
                    checkboxValues.push($(Input[i]).attr('SHNUMBER'));
                    checkboxValues.push($(Input[i]).attr('DELDATE'));
                    checkboxValues.push($(Input[i]).attr('VBELN'));
                    checkboxValues.push($(Input[i]).attr('VEHICLE'));
                    checkboxValues.push($(Input[i]).attr('AMOUNT'));
                    checkboxValues.push($(Input[i]).attr('QTY'));
                    checkboxValues.push($(Input[i]).attr('UNIT'));
                    checkboxValues.push($(Input[i]).attr('ACK'));
                    checkboxValues.push($(Input[i]).attr('F1'));
                    checkboxValues.push($(Input[i]).attr('F2'));
                    checkboxValues.push($(Input[i]).attr('F3'));
                    checkboxValues.push($(Input[i]).attr('F4'));
                    checkboxValues.push($(Input[i]).attr('F5'));
                    checkboxValues.push($(Input[i]).attr('RBLGP'));
                    vai = 'key'+ val
                    temp.push(checkboxValues)
                }
                
        });
        console.log(temp);
        console.log(checkboxValues);
        var fromDate = $("#fromDate").val();
        var toDate = $("#toDate").val();

        $.ajax({
            type: "POST",
            url: "{{ URL::to('/SubmitInvoice') }}",
            data: {_token: $('input[name=_token]').val(), ShipmentNo: temp , FromDate:fromDate, ToDate:toDate },
            success: function (data) {
                //var Data = jQuery.parseJSON(data);
                
                //Update Json File
                for(var i =0;i<temp.length;i++){
                    storedData.removeValue("SHNUMBER",temp[i][0]); //remove json row having Shipment Number
                    console.log(storedData.length)
                    if(storedData.length == 0){ 
                        storedData = "";
                    }
                    else{ 
                        storedData.splice();
                    }
                }
                console.log("stored Data for JSON File"+storedData)
                
                
                
                console.log(data);
                console.log("Submitted");
                $('#SubmitInvoice').attr('disabled','true');
                $('.messgae-containers').hide();
                if(data != 1 && data != ""){
                    FileName = {{ $UserID }}+"_"+fromDate+"_"+toDate+".json"
                
                    $.ajax
                    ({
                        type: 'POST',
                        url: "{{ URL::to('/UpdateJSON') }}",
                        data: {_token: $('input[name=_token]').val(),StoredData : storedData , FileName : FileName},
                        success: function () {
                            console.log("JSON File Updated")
                        },
                        failure: function() {
                            console.log("Error in submission!")
                        }
                    });
                    $('.messgae-containerSuccess p').text('Sap Invoice '+data+' Submitted Successfully');
                    $('.messgae-containerSuccess').show();
                }
                else{
                    if(data == 1){
                        $('.messgae-containerservererror p').text('Could not connect to host');
                        $('.messgae-containerservererror').show();
                    }
                    else{
                        $('.messgae-containerservererror p').text('Server Error');
                        $('.messgae-containerservererror').show();
                    }
                    $('#loader').hide();
                    $('#SubmitInvoice').removeAttr('disabled');
                    return false;
                }
                
                $('#TotalAmount').text('0');
                $('#TotalAmount').number( true, 2 );
                $('#SelectedInvoice').text('0');
                for (var i =0;i<temp.length;i++){
                    console.log(temp[i][0]);
                    ShipArray.splice( $.inArray(temp[i][0],ShipArray) ,1 );
                    console.log($("div[name="+temp[i][0]+"]"));
                    $("tr[name="+temp[i][0]+"]").remove();
                }
                Initial = 0;
                for (var i =0;i<temp.length;i++){
                    console.log(temp[i][0]);
                    console.log($("div[name="+temp[i][0]+"]"));
                    $("div[name="+temp[i][0]+"]").remove();
                }
                var CheckPanel = $('#invoice tr').length;
                console.log($(CheckPanel));
                if(CheckPanel != 0){ 
                    $('#loader').hide();
                }
                else{ 
                    $('.page-header').html('Invoices')
                    var HTML = "";
                    HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No data available in table</td></tr>";
                    $("#invoice").append(HTML);
                    $('#loader').hide();
                }
            }
        });
        
    });
    
    $(document).on('click', '#Print', function () {
        var divContents = $("#print").html();
        var printWindow = window.open('', '', 'height=400,width=800');
        printWindow.document.write('<html><head><title>Invoice Detail</title>');
        printWindow.document.write('</head><body >');
        printWindow.document.write(divContents);
        printWindow.document.write('</body></html>');        
        printWindow.document.close();
        printWindow.print();
    });

    $(document).on('click', '#removeinvoice', function () {
        var fromDate = $("#fromDate").val();
        var toDate = $("#toDate").val();
        $('#SubmitInvoice').attr('disabled','true');
        $('#removeinvoice').attr('disabled','true');
        $('.messgae-container').hide();
        $('.messgae-containerremove').show();
        $('#loaders').show();
        var checkboxValues = [];
        var temp = Array()
        var i=1;
        $('input[name=checkbox]:checked').map(function(val , i) {
            var Class = $(this).attr('class');
            var Input = $("."+Class+":input[name=CheckBox]");
            for(var i = 0; i <$("."+Class+":input[name=CheckBox]").length; i++){
                checkboxValues =Array();
                checkboxValues.push($(Input[i]).val());
                checkboxValues.push($(Input[i]).attr('LIFNR'));
                checkboxValues.push($(Input[i]).attr('NAME1'));
                checkboxValues.push($(Input[i]).attr('SHNUMBER'));
                checkboxValues.push($(Input[i]).attr('DELDATE'));
                checkboxValues.push($(Input[i]).attr('VBELN'));
                checkboxValues.push($(Input[i]).attr('VEHICLE'));
                checkboxValues.push($(Input[i]).attr('AMOUNT'));
                checkboxValues.push($(Input[i]).attr('QTY'));
                checkboxValues.push($(Input[i]).attr('UNIT'));
                checkboxValues.push($(Input[i]).attr('ACK'));
                checkboxValues.push($(Input[i]).attr('F1'));
                checkboxValues.push($(Input[i]).attr('F2'));
                checkboxValues.push($(Input[i]).attr('F3'));
                checkboxValues.push($(Input[i]).attr('F4'));
                checkboxValues.push($(Input[i]).attr('F5'));
                checkboxValues.push($(Input[i]).attr('RBLGP'));
                vai = 'key'+ val
                temp.push(checkboxValues)
            }    
        }); 
        
        console.log(temp);
        console.log(storedData)
        for(var i =0;i<storedData.length;i++){
            var ShipNUMBER = storedData[i].SHNUMBER;
            for(var j=0;j<temp.length;j++){
                if(ShipNUMBER == temp[j][0] ){
                    storedData[i].Status = 0;
                    break;
                }
            }
        }  
        FileName = {{ $UserID }}+"_"+fromDate+"_"+toDate+".json"
          console.log(temp);
        console.log(storedData)      
        $.ajax
        ({
            type: 'POST',
            url: "{{ URL::to('/UpdateJSON') }}",
            data: {_token: $('input[name=_token]').val(),StoredData : storedData , FileName : FileName},
            success: function () {
                console.log("JSON File Updated")
            },
            failure: function() {
                console.log("Error in submission!")
            }
        });
        $('#TotalAmount').text('0');
        $('#TotalAmount').number( true, 2 );
        $('#SelectedInvoice').text('0');
        console.log(ShipArray);
        Initial = 0;
        for (var i =0;i<temp.length;i++){
            console.log(temp[i][0]);
            ShipArray.splice( $.inArray(temp[i][0],ShipArray) ,1 );
            console.log($("div[name="+temp[i][0]+"]"));
            $("tr[name="+temp[i][0]+"]").remove();
        }
        console.log(ShipArray);
        var CheckPanel = $('#invoice tr').length;
        console.log($(CheckPanel));
        if(CheckPanel != 0){ 
            $('#loaders').hide();
        }
        else{ 
            $('.page-header').html('Invoices')
            var HTML = "";
            HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No data available in table</td></tr>";
            $("#invoice").append(HTML);
            $('#loaders').hide();
        }
    });
    
    $(document).on('click', '#addinvoice', function () {
            $('#fetchinvoice').attr('disabled','true');
            $('#loaders').show();
            
            var fromDate = $("#fromDate").val();
            var toDate = $("#toDate").val();
            
            var contractorId = $('#contractorId option:selected').attr('contractorid')
            var shipmentNo = $("#shipmentNo").val();
            var array = new Array();
            if (storedData != "") {
                for (var i = 0; i < storedData.length; i++) {
                    array.push(storedData[i].SHNUMBER);
                }
                console.log(array);
                if(jQuery.inArray(shipmentNo,array) == -1){
                    prevShipmentNo = false;
                }
                else{
                    prevShipmentNo = true;
                }
            }
            console.log(prevShipmentNo);
            var flag = false;
            $('.messgae-container').hide();

            if(jQuery.inArray(shipmentNo,ShipArray) != -1){
                $('.messgae-containerAlreadyFetch').show();
                $('#fetchinvoice').removeAttr('disabled');
                $('#loaders').hide();
                return false;
            }
           
            if (storedData != "") { 
                if($('.panel-body tbody[id="invoice"] tr').last().text().trim() == "No data available in table" || $('.panel-body tbody[id="invoice"] tr').last().text().trim() == "No Invoice"){ 
                    $('#invoice tr').remove()
                }
                $('.page-header').html('Invoices Of '+storedData[0].NAME1+'<br />Vendor Code '+storedData[0].LIFNR)
                var HTML = "";
                var count = 0;
                for (var i = 0; i < storedData.length; i++) { 
                    if (storedData[i].SHNUMBER == shipmentNo) {
                        Initial = 1;
                        var CheckID = storedData[i].F3;
                        HTML = HTML + "<tr class='"+ CheckID +"' name="+storedData[i].SHNUMBER+">";
                        HTML = HTML + "<input type='hidden' class='"+ storedData[i].SHNUMBER + "' name='CheckBox' value='"+ storedData[i].SHNUMBER + "' id='" + storedData[i].LIFNR + "' LIFNR='"+ storedData[i].LIFNR +"' NAME1='"+ storedData[i].NAME1 +"' SHNUMBER='"+ storedData[i].SHNUMBER  +"' DELDATE='"+ storedData[i].DELDATE +"' VBELN='"+ storedData[i].VBELN +"' VEHICLE='"+ storedData[i].F3 +"' AMOUNT='"+ storedData[i].AMOUNT +"' QTY='"+storedData[i].QTY +"' UNIT='"+ storedData[i].UNIT +"' ACK='"+ storedData[i].ACK +"' F1='"+ storedData[i].F1 +"' F2='"+ storedData[i].F2 +"' F3='"+ storedData[i].F3 +"' F4='"+ storedData[i].F4 +"' F5='"+ storedData[i].F5 +"' RBLGP='"+ storedData[i].RBLGP +"' />";
                        
                        $('#ShipmentRowCheck').val(++count);
                        if($('#ShipmentNOCheck').val() == 0){
                            HTML = HTML + "<td rowspan='10' class='" + storedData[i].SHNUMBER + "' style='text-align:center'><input type='checkbox' title='Check To Submit Invoice' class='"+ shipmentNo +"' name='checkbox' value='"+ shipmentNo + "' ></td>";
                            HTML = HTML + "<td rowspan='10' class='" + storedData[i].SHNUMBER + "'>" + storedData[i].SHNUMBER + "</td>";
                            $('#ShipmentNOCheck').val('1');
                        }
                        HTML = HTML + "<td>" + storedData[i].F3 + "</td>";
                        HTML = HTML + "<td>" + storedData[i].QTY + "</td>";
                        HTML = HTML + "<td>" + storedData[i].VBELN + "</td>";
                        HTML = HTML + "<td>" + storedData[i].DELDATE + "</td>";
                        HTML = HTML + "<td>" + storedData[i].F2 + "</td>";
                        HTML = HTML + "<td class='Amount'>" + storedData[i].AMOUNT + "</td>";
                        HTML = HTML + "<td><a href='javascript:void(0)' class='detail' id='" + storedData[i].LIFNR + "' LIFNR='"+ storedData[i].LIFNR +"' NAME1='"+ storedData[i].NAME1 +"' SHNUMBER='"+ storedData[i].SHNUMBER  +"' DELDATE='"+ storedData[i].DELDATE +"' VBELN='"+ storedData[i].VBELN +"' VEHICLE='"+ storedData[i].F3 +"' AMOUNT='"+ storedData[i].AMOUNT +"' QTY='"+storedData[i].QTY +"' UNIT='"+ storedData[i].UNIT +"' ACK='"+ storedData[i].ACK +"' F1='"+ storedData[i].F1 +"' F2='"+ storedData[i].F2 +"' F3='"+ storedData[i].F3 +"' F4='"+ storedData[i].F4 +"' F5='"+ storedData[i].F5 +"' RBLGP='"+ storedData[i].RBLGP +"' >Detail</a></td>";
                        HTML = HTML + "</tr>";
                        flag = true;
                        storedData[i].Status = "1";
                        
                        FileName = {{ $UserID }}+"_"+fromDate+"_"+toDate+".json"
                        
                        $.ajax
                        ({
                            type: 'POST',
                            url: "{{ URL::to('/UpdateJSON') }}",
                            data: {_token: $('input[name=_token]').val(),StoredData : storedData , FileName : FileName},
                            success: function () {
                                console.log("JSON File Updated")
                            },
                            failure: function() {
                                console.log("Error in submission!")
                            }
                        });
                    }
                }
                if(flag){
                    ShipArray.push(shipmentNo);
                }
                $('#ShipmentNOCheck').val('0');
                $("#invoice").append(HTML);
                $('.'+CheckID+' td[class="'+shipmentNo+'"]').attr('rowspan',$('#ShipmentRowCheck').val());
                $('#ShipmentRowCheck').val('0');
            }
            else{ 
                $('.messgae-containerSuccess').hide();
                $('.messgae-containers').show();
                if(Initial != 0){
                    $(".panel-body").last().remove()
                }
                else{
                    $('#invoice tr').remove()
                    $('.page-header').html('Invoices')
                    var HTML = "";
                    HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No Invoice</td></tr>";
                    $("#invoice").append(HTML);
                }
                }
            if (!flag){ 
                $('.messgae-containerSuccess').hide();
                $('.messgae-containers').show();
                if(Initial != 0){ 
                    //$(".panel-body").last().remove()
                }
                else{ 
                    $('#invoice tr').remove()
                    $('.page-header').html('Invoices')
                    var HTML = "";
                    HTML = HTML + "<tr><td valign='top' colspan='9' class='dataTables_empty'>No Invoice</td></tr>";
                    $("#invoice").append(HTML);
                }
            }
            $('#fetchinvoice').removeAttr('disabled');
            $('#loaders').hide();
            prev_contractor = $('#contractorId option:selected').attr('contractorid')
            prevFromDate = $("#fromDate").val();
            prevToDate = $("#toDate").val();
            prevShipmentNo = $("#shipmentNo").val();
            $('.Amount').number( true, 2 );
    });
    
    $(document).on('change', '.FromDate,.ToDate', function () {
        var FromDate = $("#fromDate").val();
        var ToDate = $("#toDate").val();
        
        var From =  $("#fromDate").datepicker("getDate")
        var To =  $("#toDate").datepicker("getDate")
        
        console.log(From);
        console.log(To);
        
        
        if(FromDate != "" && ToDate != "" && From > To){ 
             $('.ErrorDate').show();
             $('#fetchinvoice').attr('disabled','true')
        }
        else{
            $('.ErrorDate').hide();
            $('#fetchinvoice').removeAttr('disabled')
        }
    });
    
</script>