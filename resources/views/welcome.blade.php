@extends('layouts.app')
@section('content')
<link href="{{ URL::to('/public/js/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ URL::to('/public/js/bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">
<link href="{{ URL::to('/public/js/dist/css/timeline.css') }}" rel="stylesheet">
<link href="{{ URL::to('/public/js/dist/css/sb-admin-2.css') }}" rel="stylesheet">
<link href="{{ URL::to('/public/js/bower_components/morrisjs/morris.css') }}" rel="stylesheet">
<link href="{{ URL::to('/public/js/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>

    </div>
    <div class="row">
        @if($Data['UserType'] == 'superadmin')
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary" title="Number Of Admin">
                <div class="panel-heading backgroundgreen">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $Data['Admin'] }}</div>
                            <div>Admins</div>
                        </div>
                    </div>
                </div>
<!--                <a href="{{ URL::to('/adminlist') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Show</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
        @endif
        @if($Data['UserType'] == 'superadmin' || $Data['UserType'] == 'admin')
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green" title="Number Of Super/Power User">
                <div class="panel-heading backgroundblue">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $Data['SuperUser'] }}</div>
                            <div>Super Users</div>
                        </div>
                    </div>
                </div>
<!--                <a href="{{ URL::to('/superuserlist') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Show</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
        @endif
        @if($Data['UserType'] == 'superadmin' || $Data['UserType'] == 'admin' || $Data['UserType'] == 'superuser')
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow" title="Number Of Contractor/Vendor">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $Data['Contractor'] }}</div>
                            <div>Contractors</div>
                        </div>
                    </div>
                </div>
<!--                <a href="{{ URL::to('/contractorlist') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Show</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
        @endif
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red" title="Number Of Submitted Invoice">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-book fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">{{ $Data['Invoice'] }}</div>
                            <div>Invoices</div>
                        </div>
                    </div>
                </div>
<!--                <a href="{{ URL::to('/submittedinvoices') }}">
                    <div class="panel-footer">
                        <span class="pull-left">Show</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>-->
            </div>
        </div>
    </div>
</div>
@endsection
<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>