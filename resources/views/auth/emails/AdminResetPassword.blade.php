<div style="display: block;margin: 0 auto;">
    <div style="width: 50%;border:solid 1px black;border-radius: 5px;margin-left: 25%;background: #009d57" >
        <div style="border-radius: 5px;padding: 0px 0px 0px 10px;" >
            <p style="color:white">You have Reset {{ $Users->UserName }} ({{ $Users->Email }}) Password.The new Password is</p>
        </div>
        <div style="background: white;padding: 20px 20px 20px 20px;" >
            <div style="text-align: center">
                <p style="display: inline-block;padding: 6px 12px;margin-bottom: 0;font-size: 14px;font-weight: 400;line-height: 1.42857143;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action: manipulation;touch-action: manipulation;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background-image: none;border: 1px solid transparent;border-radius: 4px;text-decoration: none;color: #fff!important;background-color: #00458d!important;border-color: #00458d!important">{{ $Password }}</p>
            </div>
        </div>
    </div>
</div>