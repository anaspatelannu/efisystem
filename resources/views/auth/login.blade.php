@extends('layouts.app')
@section('content')
<?php  
    //$encrypter = app('Illuminate\Encryption\Encrypter');
    //$encrypted_token = $encrypter->encrypt(csrf_token());
?>
<style>
    .backgroundcolorgreen{
        background-color: #009d57!important;
        background-image: url("{{ url('/public/images/slider5.jpg') }}");
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        @if($Login == 0 || $Login == 3)
                        <div class="messgae-container">
                            <div class="status-message error-msg">
                                <p class="message">{{ $Msg }}</p>
                            </div>
                        </div>
                        @endif
                        @if($Login == 2)
                        <div class="messgae-container">
                            <div class="status-message success-msg">
                                <p class="message">{{ $Msg }}</p>
                            </div>
                        </div>
                        @endif
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" required="true" type="email" class="form-control" name="email" autocomplete="off" autofocus="true" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" required="true" type="password" class="form-control" autocomplete="off" name="password">

                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <!--                        <div class="form-group">
                                                    <div class="col-md-6 col-md-offset-4">
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="remember"> Remember Me
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>-->

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>
                                <a class="btn btn-link" href="{{ url('/ForgotPassword?ID=1') }}">Forgot Password</a>
                                @if($Login == 0)
                                <a class="btn btn-link" href="{{ url('/ForgotPassword?ID=2') }}">Reset Password</a>
                                <!--<a class="btn btn-link" href="{{ url('/password/reset') }}">Reset Password</a>-->
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>-->
<script src="{{ URL::to('/public/js/jquery2.2.3-min.js') }}"></script>
<script>
    $(document).ready(function () {
      $('#email').val('');  
      $('#password').val('');  
    })
</script>
