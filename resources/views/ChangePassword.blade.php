@extends('layouts.app')
@section('content')
<style>
    .backgroundcolorgreen{
        background-color: #009d57!important;
        background-image: url("{{ url('/public/images/slider5.jpg') }}");
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading backgroundgreen colorwhite">Change Password</div>
                <div class="panel-body">
                    @if($Msg != "")
                        <div class="alert alert-success" style="text-align:center">
                            {{ $Msg }}
                        </div>
                    @endif
                    @if($Msg1 != "")
                        <div class="alert alert-danger" style="text-align:center">
                            {{ $Msg1 }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/ChangePassword?ID='.$ID.'&Email='.$Email) }}">
                        {{ csrf_field() }}
                        @if($ID == 2)
                        <div class="form-group{{ $errors->has('oldpassword') ? ' has-error' : '' }}">
                            <label for="oldpassword" class="col-md-4 control-label">Old Password</label>

                            <div class="col-md-6">
                                <input id="oldpassword" type="password" class="form-control" name="oldpassword" value="{{ old('oldpassword') }}">

                                @if ($errors->has('oldpassword'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('oldpassword') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        @endif
                        <div class="form-group{{ $errors->has('newpassword') ? ' has-error' : '' }}">
                            <label for="newpassword" class="col-md-4 control-label">New Password</label>

                            <div class="col-md-6">
                                <input id="newpassword" type="password" required="true" class="form-control" name="newpassword" value="{{ old('oldpassword') }}">

                                @if ($errors->has('newpassword'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('newpassword') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('confpassword') ? ' has-error' : '' }}">
                            <label for="confpassword" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="confpassword" type="password" required="true" class="form-control" name="confpassword" value="{{ old('oldpassword') }}">

                                @if ($errors->has('confpassword'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('confpassword') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-lock"></i> Change Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

<script src="{{ URL::to('/public/js/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::to('/public/js/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>
