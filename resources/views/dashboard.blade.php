@extends('layouts.app')
@section('content')

<div id="wrapper">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">User Management</h1>
            </div>
            <div>
                <div class="containers">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="panel-default">

                                <div class="panel-body">
                                    @if($Msg != "")
                                        <div class="alert alert-success" style="text-align:center">
                                            {{ $Msg }}
                                        </div>
                                    @endif
                                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/createuser') }}">
                                        {{ csrf_field() }}

                                        <div class="form-group{{ $errors->has('usertype') ? ' has-error' : '' }}">
                                            <label for="usertype" class="col-md-4 control-label">User Type</label>

                                            <div class="col-md-6">
                                                <!--<input id="usertype" type="number" class="form-control" name="usertype" value="{{ old('usertype') }}">-->
                                                @if(session()->get('Name') == "superadmin")
                                                <select id="usertype" class="form-control" name="usertype" value="{{ old('usertype') }}">
                                                    <option value="admin">Admin</option>
                                                    <option value="superuser">Reviewer</option>
                                                    <option value="contractor">Contractor</option>
                                                </select>
                                                @elseif(session()->get('Name') == "admin")
                                                <select id="usertype" class="form-control" name="usertype" value="{{ old('usertype') }}">
                                                    <option value="superuser">Reviewer</option>
                                                    <option value="contractor">Contractor</option>
                                                </select>
                                                @endif
                                                @if ($errors->has('usertype'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('usertype') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <label for="username" class="col-md-4 control-label">User Name</label>

                                            <div class="col-md-6">
                                                <input id="username" type="text" class="form-control" required="true" name="username" value="{{ old('username') }}">

                                                @if ($errors->has('username'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('username') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                            <div class="col-md-6">
                                                <input id="email" type="email" required="true" class="form-control" name="email" value="{{ old('email') }}">

                                                @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <label for="password" class="col-md-4 control-label">Password</label>

                                            <div class="col-md-6">
                                                <input id="password" type="password" required="true" class="form-control" name="password">

                                                @if ($errors->has('password'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div id="VendorCode" class="form-group{{ $errors->has('vendorcode') ? ' has-error' : '' }}">
                                            <label for="vendorcode" class="col-md-4 control-label">Vendor Code</label>

                                            <div class="col-md-6">
                                                <input id="vendorcode" type="text" class="form-control" name="vendorcode" value="{{ old('vendorcode') }}">

                                                @if ($errors->has('vendorcode'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('vendorcode') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>
                                        
                                        <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                            <label for="address" class="col-md-4 control-label">Address</label>

                                            <div class="col-md-6">
                                                <input id="address" type="test" required="true" class="form-control" name="address" value="{{ old('address') }}">

                                                @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group{{ $errors->has('contactnumber') ? ' has-error' : '' }}">
                                            <label for="contactnumber" class="col-md-4 control-label">Contact Number</label>

                                            <div class="col-md-6">
                                                <input id="contactnumber" type="number" required="true" class="form-control" name="contactnumber" value="{{ old('contactnumber') }}">

                                                @if ($errors->has('contactnumber'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('contactnumber') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-6 col-md-offset-4">
                                                <button type="submit" title="Create Admin/Super User/Contractor" id="create" class="btn btn-primary">
                                                    <i class="fa fa-btn fa-plus-square"></i> Create
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>-->
<script src="{{ URL::to('/public/js/jquery2.0.0.min.js') }}"></script>
<script>
$(document ).ready(function() {
    var UserType = $('#usertype').val();
    if(UserType != 'contractor'){
        $('#VendorCode').hide();
    }
});
    
$(document).on('focusout', '#email', function () {
    if($('.messgae-container p').text() == "User Name Already Exists" || $('.messgae-container p').text() == "Vendor Code Already Exists"){
        return false;
    }
    var Email = $(this).val();
    $.ajax({
        type: "POST",
        url: "{{ URL::to('/checkemail') }}",
        data: {Email: Email, _token: $('input[name=_token]').val()},
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $('.messgae-container p').text('Email Already Exists')
                $('.messgae-container').show();
                $('#create').attr('disabled','true');
                $("#email").focus();
                return false;
            }
            else {
                $('.messgae-container').hide();
                $('.messgae-container p').text('');
                $('#create').removeAttr('disabled');
            }
        }
    });
});

$(document).on('focusout', '#username', function (e) {
    if($('.messgae-container p').text() == "Email Already Exists" || $('.messgae-container p').text() == "Vendor Code Already Exists"){
        return false;
    }
    var UserName = $(this).val();
    $.ajax({
        type: "POST",
        url: "{{ URL::to('/checkusername') }}",
        data: {UserName: UserName, _token: $('input[name=_token]').val()},
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $('.messgae-container p').text('User Name Already Exists')
                $('.messgae-container').show();
                $('#create').attr('disabled','true');
                $("#username").focus();
                e.preventDefault();
                return false;
            }
            else {
                $('.messgae-container').hide();
                $('.messgae-container p').text('');
                $('#create').removeAttr('disabled');
            }
        }
    });
});

$(document).on('focusout', '#vendorcode', function (e) {
    if($('.messgae-container p').text() == "User Name Already Exists" || $('.messgae-container p').text() == "Email Already Exists"){
        return false;
    }
    var VendorCode = $(this).val();
    $.ajax({
        type: "POST",
        url: "{{ URL::to('/CheckVendorCode') }}",
        data: {VendorCode: VendorCode, _token: $('input[name=_token]').val()},
        success: function (data) {
            console.log(data);
            if (data == 1) {
                $('.messgae-container p').text('Vendor Code Already Exists')
                $('.messgae-container').show();
                $('#create').attr('disabled','true');
                $("#vendorcode").focus();
                e.preventDefault();
                return false;
            }
            else {
                $('.messgae-container').hide();
                $('.messgae-container p').text('');
                $('#create').removeAttr('disabled');
            }
        }
    });
});

$(document).on('change', '#usertype', function (e) {
    $('.messgae-container').hide();
    var UserType = $('#usertype').val();
    if(UserType != 'contractor'){
        $('#VendorCode').hide();
        $('#vendorcode').removeAttr('required');
    } 
    else{
        $('#VendorCode').show();
        $('#vendorcode').attr('required','true');
    }
    
});
</script>
